(*
    Vector Battle
    Copyright (C) 2006 - 2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

*)

(*

*  What does this file do?

**    Provides type and class for the event system

*)



type input_event =
| Down
| Up
| Left
| Right
| Fire
| Jump
| Change
| Rope
| Nothing
| Quit
;;

(*
let event_list =
   [Down; Up; Left; Right; Fire; Jump; Change;
    Rope; Nothing]
;;
*)

class event_table =
   object
      val ev = Hashtbl.create 8
      method hard_init =
         Hashtbl.add ev Down false;
         Hashtbl.add ev Up false;
         Hashtbl.add ev Left false;
         Hashtbl.add ev Right false;
         Hashtbl.add ev Fire false;
         Hashtbl.add ev Jump false;
         Hashtbl.add ev Change false;
         Hashtbl.add ev Rope false;
      method press key =
         Hashtbl.replace ev key true
      method release key =
         Hashtbl.replace ev key false
      method includes key =
         try
            Hashtbl.find ev key
         with Not_found -> false
   end
;;

let new_event_table () = 
   let nev = new event_table in
      nev#hard_init;
      nev
;;
