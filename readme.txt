Hello to anyone who might read this,

If you want to read through any of the code, here's a basic outline of 
the structure.

vectorb.ml : This is the toplevel file of the program; it relies on 
all the other files in the program, and nothing relies on it. As with 
many programming languages, it's easiest to begin with the final 
function in the toplevel file and work from there.

api_ui.ml : This is the toplevel of the user interface. It has lots of 
messy menu-rendering and question-asking functions, as well as various 
functions for putting things on the screen. All of the low-level user 
interface stuff is passed on to the backend.

uilink.ml : This links the toplevel user interface code with the backend 
code. I think this file may actually be structurally redundant and could 
in theory be factored out, but there's no real reason to do so. The only 
user interface backend which I've maintained in this branch of the code 
is SDL, but I have written two other complete backends for the 
Roguelike branch of the code, so I know the decoupling works.
   Edit - the user interface has changed and expanded substantially 
since then, so while in theory the "wrapping up" of the UI has been 
maintained, I don't know how much work it would actually be to write 
another backend now. I think that in order to do it properly, I should 
really learn how to write those .mli interface files in order to reduce 
the amount of redundant code.

collide.ml : Contains some collision-detection code. Resolution is 
handled in the colliding object.

algo.ml : Contains implementations of various common algorithms, 
including Bresenham's line drawing algorithm. Probably contains some 
redundant stuff from the Roguelike project.

As for the rest of it, feel free to ask questions, but the above will 
probably give you a good outline of how everything hangs together. 
Throughout the non-UI code, things like graphical images and blitting 
surfaces are always referred to by strings, in order to keep the rigid 
decoupling between UI and everything else. This is probably very 
inefficient, but it seemed to provide the structure I wanted without 
making the code too unreadable.

To compile, you will need:

libsdl-ocaml-dev libsdl-image1.2-dev libsdl-ttf2.0-dev libsdl1.2-dev
ocaml ocaml-findlib ocaml-native-compilers

You'll also need other standard development tools like _make_ and a 
linker and so on, but you probably have them already. :)

To compile and run the bytecode version of Vector Battle, type from the 
source directory:

make && ./byte

Or for the native code version:

make native && ./native

The keysets which (sort of) work in this version are :

a)

  r
 dfg
zxc

with r and f being aim up/ aim down, d and g being move left/right, c
being fire, x being jump, and z is "change" but that doesn't do anything 
yet. 

b)

 up, down, left right, comma, dot, slash, in the same order as laid out 
above.

There is an options file named vectorb.rc, in which you can set 
fullscreen mode among other things. Playing with the values may give 
strange results; a better option-parsing system will be needed before 
final release.

The map is just an image file, with black being empty space and every 
other colour being rock (at this stage). A more sophisticated map-making 
system is one of the planned features; it will take a materials image, 
an overlay image, and a background image instead of just the one. Other 
planned features are mission mode, network play, co-operative missions, 
and maybe some competitive game modes besides just deathmatches. Weapon 
editor and mission editor are other planned features. Always too many 
planned features. :)

Best wishes,

-- 
--jude.
