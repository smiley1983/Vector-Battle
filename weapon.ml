(*
    Vector Battle
    Copyright (C) 2004-2010  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Provides classes for weapons and projectiles

*)

type bullet_specs =
 {
   pname : string;
   launch_on_explosion : string list;
   launch_trail : string list;
   trail_delay : float;
   gravity : float;
   die_on_wormhit : bool;
   is_blood : bool;
   die_out_of_bounds : bool;
   width : int;
   height : int;
   anim_cycles : int;
   anim_speed : float;
   anim_style : string;
   anim_start_frame : int;
   image : string;
   rope : bool;
   sticky : float;
   explodes_on_ground : bool;
   explodes_on_timeout : bool;
   explodes_on_wormhit : bool;
   length : float;
   bounce : float;
   damage : float;
   damage_pulse : float;
   time_to_live : float;
 }
;;

type launcher =
 {
   wname : string;
   l_bullet : bullet_specs;
   shots_at_once : int;
   shots_per_reload : int;
   shot_delay : float;
   shot_spread : float;
   shot_speed : float;
   scatter : string;
   reload_time : float;
   kick : float;
 }
;;

type weaponslot =
 {
   l : launcher;
   mutable shots_left : int;
   mutable reloading : float;
 }
;;

class projectile lx ly spec_r firing_player
 =
   object(self)
      val mutable locx = lx
      val mutable locy = ly
      val mutable sprite_locx = 0
      val mutable sprite_locy = 0
      val mutable veloc_x = 0.0
      val mutable veloc_y = 0.0
      val mutable ttl = spec_r.time_to_live
      val mutable current_anim_speed = spec_r.anim_speed
      val mutable current_anim_cycle = 0 (*FIXME*)
      val mutable anim_delay = 0.0
      val mutable damage_delay = 0.0
      val mutable ntrail_delay = spec_r.trail_delay
      val owner = (firing_player: int)
      val mutable alive = true
      val mutable stuck = spec_r.sticky
      val mutable deco = spec_r.is_blood
      val specs = spec_r
      method move elapsed =
(*
         (*DEBUG*) print_string ("\n\nMoved in "
               ^ (string_of_float elapsed) ^ " by " ^ 
               (string_of_float (veloc_x *. elapsed)) ^ 
               " x " ^ 
               (string_of_float (veloc_y *. elapsed)) ^ 
               " to " ^ (string_of_float locx) ^ 
               " x " ^ (string_of_float locy) ^
               "\n");
*)
         locx <- locx +. ( veloc_x *. elapsed);
         locy <- locy +. ( veloc_y *. elapsed);
(* *)
         if not (damage_delay <= 0.0) then
           (
            damage_delay <- max 0.0 (damage_delay -. elapsed)
           );
         if not (ttl = 0.0) then
           (
            ttl <- ttl -. elapsed;
            if ttl <= 0.0 then ttl <- -1.0
           );
         if not (ntrail_delay <= 0.0) then
           (
            ntrail_delay <- ntrail_delay -. elapsed;
           );
         match specs.anim_style with
          | "aim" ->
               if specs.anim_cycles > 0 then
               let calc_dir = Algo.vector veloc_x veloc_y in
(*
               let dir = mod_float (calc_dir +. 157.5) 360. in 
*)
               let segsize = 360. /. (float_of_int specs.anim_cycles) in
               let dir = (calc_dir +. (segsize *. 2. /. 3.)) +. 360. in 
                  current_anim_cycle <- 
                     (int_of_float (dir /. segsize))
                     mod specs.anim_cycles
          | "spin"
          | "loop" ->
               if (specs.anim_cycles > 0) && (current_anim_speed > 0.0) 
               then
                 (
                  if anim_delay <= 0.0 then
                    (
    (*               print_string "yes\n"; *)
                     current_anim_cycle <- (current_anim_cycle + 1);
                     anim_delay <- specs.anim_speed
                        (*current_anim_speed*)
                    );
                  if current_anim_cycle > specs.anim_cycles then
                    current_anim_cycle <- specs.anim_start_frame;
                  anim_delay <- anim_delay -. elapsed;
                 )
          | "none"
          | _ -> ()
(* *)
      method gravity_pull grav elapsed =
         veloc_y <- veloc_y +. (specs.gravity *. grav *. elapsed)
      method drag elapsed amount =
      (* Note that "amount" refers to how much velocity you want to 
         _remove_ *)
         veloc_x <- veloc_x *. (1. -. (amount *. elapsed));
         veloc_y <- veloc_y *. (1. -. (amount *. elapsed));
      method in_bounds x_boundary y_boundary =
         if (int_of_float locx) < 0 || (int_of_float locy) < 0 
         || (int_of_float locx) > x_boundary 
         || (int_of_float locy) > y_boundary then 
           (
(*
            (*DEBUG*) print_string ("\noob:" ^ (string_of_float locx) ^ 
                  " x " ^ (string_of_float locy) ^ "\n\n");
*)
            false
           )
         else true
      method get_graphic = specs.image
      method get_rect = self#get_img_offset
      method get_locx = int_of_float (locx +. 0.5)
      method get_locy = int_of_float (locy +. 0.5)
      method get_int_loc = self#get_locx, self#get_locy
      method internal_get_locx = locx 
      method internal_get_locy = locy 
      method set_locx n = locx <- n
      method set_locy n = locy <- n
      method set_veloc_x n = veloc_x <- min 200. n
      method set_veloc_y n = veloc_y <- min 200. n
      method dead_stop = 
         veloc_x <- 0.0; veloc_y <- 0.0; 
         if specs.anim_style = "spin" then
           (
(*
            print_string "stopped";
*)
            current_anim_speed <- 0.0;
           )
      method get_veloc_x = veloc_x 
      method get_veloc_y = veloc_y 
      method confirm_pos =
         sprite_locx <- self#get_locx;
         sprite_locy <- self#get_locy
      method get_sprite_locx = sprite_locx
      method get_sprite_locy = sprite_locy
      method get_sprite_centre =
         (self#get_sprite_locx + ((specs.width + 1) / 2)),
         (self#get_sprite_locy + ((specs.height + 1) / 2))
      method is_alive = alive
      method is_expired = (ttl = (-1.0)) 
         (*&& (specs.time_to_live > 0.0)*)
      method dies_on_wormhit = specs.die_on_wormhit
      method dies_out_of_bounds = specs.die_out_of_bounds
      method kill = alive <- false
      method explode (cpoints:(int * int) list) = 
         let (ielx, iely) = 
            List.nth cpoints (Random.int (List.length cpoints))
         in
         let elx = float_of_int ielx in let ely = float_of_int iely in
         if specs.launch_on_explosion = [] then [] else
            [(specs.launch_on_explosion, elx, ely, veloc_x, veloc_y, 
              owner)]
      method trail_now = 
         if (ntrail_delay > 0.) || (specs.launch_trail = []) then [] 
         else
           (
            ntrail_delay <- ntrail_delay +. specs.trail_delay;
            let elx, ely = self#get_sprite_centre in
            let flx = float_of_int elx in let fly = float_of_int ely in
               [(specs.launch_trail, flx, fly, 
                 veloc_x, veloc_y, owner)]
           )
      method get_bitmask_coords = (self#get_locx, self#get_locy)
      method set_is_blood b = deco <- b
      method is_blood = deco
      method get_img_rect =
         self#get_locx, self#get_locy, specs.width, specs.height
      method get_owner = owner
      method is_rope =
         specs.rope
      method set_sticky n =
         stuck <- n
      method get_sticky = stuck
      method get_length = specs.length
      method get_bounce = specs.bounce
      method get_damage = if specs.damage_pulse = -1. then specs.damage
         else specs.damage /. (specs.time_to_live /. specs.damage_pulse)
(*      method get_damage_pulse = specs.damage_pulse *)
      method damage_now = 
        ((specs.damage > 0.0) && (damage_delay <= 0.0))
        || specs.explodes_on_wormhit
      method reset_damage_pulse = damage_delay <- specs.damage_pulse
      method get_anim_start_frame = specs.anim_start_frame
(*
      method has_property p =
         (List.mem_assoc p specs.property)
*)
      method explodes_on_ground =
         specs.explodes_on_ground
      method explodes_on_timeout = specs.explodes_on_timeout
      method explodes_on_wormhit = specs.explodes_on_wormhit
(*
      method property_strength p =
         if List.mem_assoc p specs.property then
            !(List.assoc p specs.property)
         else -1.
*)
(*
      method set_property_strength p v =
         if List.mem_assoc p specs.property then
            (List.assoc p specs.property) := v
*)
      method get_img_offset =
         (specs.width * current_anim_cycle), 0, specs.width, 
               specs.height
      method set_anim_cycle v = current_anim_cycle <- v
      method get_anim_cycles = specs.anim_cycles
      method set_time_to_live n = ttl <- n
      method get_time_to_live = ttl
      method set_anim_speed n = current_anim_speed <- n
      method get_anim_speed = current_anim_speed
   end
;;

let proto_rope =
  {
   pname = "rope";
   launch_on_explosion = [];
   launch_trail = [];
   trail_delay = -1.;
   gravity = 1.0; 
   die_on_wormhit = false;
   is_blood = false;
   die_out_of_bounds = true;
   width = 1;
   height = 1;
   anim_cycles = 0;
   anim_speed = 0.0;
   anim_style = "";
   anim_start_frame = 0;
   image = "hook";
(*   bullet = false; *)
   rope = true;
   sticky = 1.0;
   explodes_on_ground = false;
   explodes_on_timeout = false;
   explodes_on_wormhit = false;
   length = 20.0;
   bounce = 200.0;
   damage = 0.0;
   damage_pulse = -1.0;
   time_to_live = 0.0;
  }
;;

let new_rope direction x y force pre_velocx pre_velocy
      fplayer 
=
   let n = new projectile
         (x -. 0.5) (y -. 0.5) (* 1 1 *) proto_rope fplayer
   in
      n#set_veloc_x (((Algo.degsin direction) *. force) +. 
pre_velocx);
      n#set_veloc_y (((Algo.degcos direction) *. force) +. pre_velocy);
      n
;;

let proto_bullet =
  {
   pname = "proto_bullet";
   launch_on_explosion = ["explosion7"];
   launch_trail = [];
   trail_delay = -1.;
   gravity = 1.0; 
   die_on_wormhit = true;
   is_blood = false;
   die_out_of_bounds = true;
   width = 1;
   height = 1;
   anim_cycles = 0;
   anim_speed = 0.0;
   anim_style = "";
   anim_start_frame = 0;
   image = "pixel";
(*   bullet = true; *)
   rope = false;
   sticky = -1.0;
   explodes_on_ground = true;
   explodes_on_timeout = false;
   explodes_on_wormhit = false;
   length = -1.0;
   bounce = -1.0;
   damage = 1.0;
   damage_pulse = -1.0;
   time_to_live = 0.0;
  }
;;

let blood_droplet =
  {
   proto_bullet with
      pname = "blood_droplet";
      launch_on_explosion = [];
      launch_trail = [];
      trail_delay = -1.;
      is_blood = true;
      anim_cycles = 31;
      anim_speed = 0.0;
      anim_style = "";
      anim_start_frame = -1;
      image = "blood_pixel";
      die_on_wormhit = false;
(*      bullet = false; *)
      explodes_on_ground = false;
      explodes_on_timeout = false;
      explodes_on_wormhit = false;
      damage = 0.0
  }
;;

let match_bullet n w =
   if List.mem_assoc n w then
      List.assoc n w
   else 
     (
      print_string ("Could not find bullet: " ^ n ^ "\n");
      proto_bullet
     )
;;

let new_projectile direction x y force pre_velocx pre_velocy 
   spec_r fplayer 
=
   let n = (new projectile
         (x -. ((float_of_int spec_r.width) /. 2.)) 
         (y -. ((float_of_int spec_r.height) /. 2.)) spec_r 
          fplayer)
   in
      n#set_veloc_x (((Algo.degsin direction) *. force) +. 
            pre_velocx);
      n#set_veloc_y (((Algo.degcos direction) *. force) +. 
            pre_velocy);
      let start_frame = n#get_anim_start_frame in
      if start_frame = (-1) then
        (
         let cycles = n#get_anim_cycles in
         if cycles > 0 then n#set_anim_cycle (Random.int cycles)
        )
      else n#set_anim_cycle start_frame;
      n
;;

let rec real_fire_launcher l direction x y pre_velocx pre_velocy 
      fplayer 
=
   next_fire_launcher l direction x y pre_velocx pre_velocy 
         fplayer l.shots_at_once
and next_fire_launcher l direction x y pre_velocx pre_velocy 
      fplayer count
=
   match count with
    | 0 -> []
    | n ->
         let mpvelocx, mpvelocy, force, mdir =
          if l.scatter = "cloud1" then
            (
             let angle = Random.float 360. in
             let fsqrt = sqrt (Random.float l.shot_speed) in
             let f = fsqrt in
             let velocx, velocy = Algo.gun_line angle f in
                pre_velocx +. velocx, pre_velocy +. velocy, 0., 0.
            )
          else if l.scatter = "static_cloud1" then
            (
             let angle = Random.float 360. in
             let fsqrt = sqrt (Random.float l.shot_speed) in
             let f = fsqrt in
             let velocx, velocy = Algo.gun_line angle f in
                velocx, velocy, 0., 0.
            )
          else if l.scatter = "arc" then
            (
             let new_direction = (direction -. (l.shot_spread /. 2.)) +.
                   (Random.float l.shot_spread)
             in pre_velocx, pre_velocy, l.shot_speed, new_direction
            )
          else if l.scatter = "diverge1" then
            (
(*
             let new_direction = (direction -. (l.shot_spread /. 2.)) +.
                   (Random.float l.shot_spread)
             in
*)
             let subspread = l.shot_spread /. 15. in
             let velocx = (0. -. (subspread /. 2.)) +.
                (Random.float subspread)
             in
             let velocy = (0. -. (subspread /. 2.)) +.
                (Random.float subspread)
             in
                pre_velocx +. velocx, 
                pre_velocy +. velocy, l.shot_speed, direction
            )
          else if l.scatter = "static" then 0.0, 0.0, 0.0, direction
          else pre_velocx, pre_velocy, l.shot_speed, direction
          in
           new_projectile mdir x y force mpvelocx 
               mpvelocy l.l_bullet fplayer
           :: next_fire_launcher l direction x y pre_velocx 
               pre_velocy fplayer (n - 1)
;;

let fire_launcher l direction x y pre_velocx pre_velocy 
      fplayer 
=
   real_fire_launcher l.l direction x y pre_velocx pre_velocy fplayer
;;

let proto_explosion =
  {
   pname = "proto_explosion";
   launch_on_explosion = [];
   launch_trail = [];
   trail_delay = -1.;
   gravity = 0.0; 
   die_on_wormhit = false;
   is_blood = false;
   die_out_of_bounds = false;
   width = 7;
   height = 7;
   anim_cycles = 7;
   anim_speed = 100000.0;
   anim_style = "loop";
   anim_start_frame = 0;
   image = "explosion7";
(*   bullet = false; *)
   rope = false;
   sticky = -1.0;
   explodes_on_ground = true;
   explodes_on_timeout = false;
   explodes_on_wormhit = false;
   length = -1.0;
   bounce = -1.0;
   damage = 1.0;
   damage_pulse = 1.0;
   time_to_live = 3.0;
  }
;;

let proto_dig_b =
  {
   proto_explosion with 
      image = "9pinvis";
      width = 9;
      height = 9;
      anim_cycles = 0;
      anim_speed = 0.0;
      anim_style = "";
      anim_start_frame = 0;
      damage = 0.0;
      damage_pulse = -1.0;
  }
;;

let proto_dig_l =
 {
   wname = "proto_dig";
   l_bullet = proto_dig_b;
   shots_at_once = 1;
   shots_per_reload = 1;
   shot_delay = 1.0;
   shot_spread = 0.0;
   shot_speed = 0.0;
   scatter = "stationary";
   reload_time = 1.0;
   kick = 0.0;
 }
;;

let proto_dig =
 {
   l = proto_dig_l;
   shots_left = 1;
   reloading = 0.0;
 }
;;

(*
let proto_large_dig =
  {
   proto_dig with 
      image = "20pinvis";
      width = 20;
      height = 20;
  }
;;
*)

(*
let new_explosion x y pattern spec_r fplayer =
   let n = new projectile
         (x -. ((float_of_int spec_r.width) /. 2.0)) 
         (y -. ((float_of_int spec_r.height) /. 2.0))
         spec_r fplayer
   in
      match pattern with 
       | "stationary" -> n
       | "cloud1" -> 
           (
            let angle = Random.float 360. in
            let force = Random.float 2. in
            let velocx, velocy = Algo.gun_line angle force in
            n#set_veloc_x velocx; n#set_veloc_y velocy;
            n 
           )
       | _ -> n
;;
*)
(*
let create_explosion bullets t x y pattern fp =
   if List.mem_assoc t bullets then
      let explosion = List.assoc t bullets in
         [new_explosion x y pattern explosion fp]
   else []
;;
*)
let rec make_new_particles_mk2 m_wpnlist ll =
   match ll with
      [] -> []
    | (lname, x, y, vx, vy, fp) :: tail ->
         let particles = ref [] in
         for count = 0 to (List.length lname - 1) do
         if List.mem_assoc (List.nth lname count) m_wpnlist then
            let launcher = List.assoc (List.nth lname count) m_wpnlist 
            in
               particles := !particles
               @ real_fire_launcher launcher 0.0 x y vx vy fp
         else print_string 
               ("Not found: " ^ (List.nth lname count) ^ "\n")
         done;
         !particles @ make_new_particles_mk2 m_wpnlist tail
;;
(*
let rec make_new_particles m_bulletlist p =
   match p with
      [] -> []
    | (nm, x, y, pattern, fp) :: tail ->
         create_explosion m_bulletlist nm x y pattern fp 
            @ make_new_particles m_bulletlist tail
;;
*)
(*
let new_blood_droplet direction x y force pre_velocx pre_velocy =
   new_projectile direction x y force pre_velocx 
         pre_velocy blood_droplet 0
;;
*)

let rec fastest l =
   next_fproj l 0.0
and next_fproj l n =
   match l with
      [] -> n
    | head :: tail ->
         let vx, vy = head#get_veloc_x, head#get_veloc_y in
         let v = (vx *. vx) +. (vy *. vy) in
            let m = max v n in
               next_fproj tail m
;;

let rec find_duplicate player l =
   match l with
      [] -> []
    | head :: tail ->
         if (head#is_rope)
         && head#get_owner = player then
            head :: find_duplicate player tail
         else find_duplicate player tail
;;

let rec identify_duplicate_ropes newlist masterlist =
   match newlist with
      [] -> []
    | head :: tail ->
         if (head#is_rope) then
            find_duplicate head#get_owner masterlist
            @ identify_duplicate_ropes tail masterlist
         else identify_duplicate_ropes tail masterlist
;;

let deplete_ammo l = l.shots_left <- (l.shots_left - 1);;

let reset_shots l = l.shots_left <- l.l.shots_per_reload;;

let get_wname w = w.l.wname;;

let get_shots_left w = w.shots_left;;

let get_shots_per_reload w = w.l.shots_per_reload;;

let get_shot_delay w = w.l.shot_delay

let reload w = 
   reset_shots w;
   w.reloading <- w.l.reload_time;
;;

let get_reloading_time w = w.reloading;;

let set_reload w n = w.reloading <- n

let get_reload_from w = w.l.reload_time;;

let get_kick w = w.l.kick;;

let uzi =
 {
   wname = "uzi";
   l_bullet = proto_bullet;
   shots_at_once = 1;
   shots_per_reload = 50;
   shot_delay = 4.0;
   shot_spread = 0.4;
   shot_speed = 3.5;
   scatter = "arc";
   reload_time = 100.0;
   kick = 1.0;
 }
;;

let bloodgun_l =
 {
   wname = "bloodgun";
   l_bullet = blood_droplet;
   shots_at_once = 8;
   shots_per_reload = 1;
   shot_delay = 1.0;
   shot_spread = 360.0;
   shot_speed = 0.3;
   scatter = "cloud1";
   reload_time = 1.0;
   kick = 1.0;
 }
;;

let bloodgun =
 {
   l = bloodgun_l;
   shots_left = 1;
   reloading = 0.0;
 }
;;

let bloodgun_with_shots n =
 {
   l = {bloodgun_l with shots_at_once = n};
   shots_left = 1;
   reloading = 0.0;
 }
;;

let shotgun =
 {
   wname = "shotgun";
   l_bullet = proto_bullet;
   shots_at_once = 15;
   shots_per_reload = 2;
   shot_delay = 30.0;
   shot_spread = 15.0;
   shot_speed = 8.0;
   scatter = "arc";
   reload_time = 120.0;
   kick = 20.0;
 }
;;

let load_bulletfile filename unused_list =
   let rbname = ref "" in
   let rcoe = ref [] in
   let rltrail = ref [] in
   let rtdelay = ref 0.0 in
   let rnumcoe = ref 0 in
   let rscatter = ref "" in
   let rgrav = ref 0.0 in
   let rdowh = ref true in
   let rdeco = ref false in
   let rdoob = ref true in
   let rwdt = ref 0 in
   let rhgt = ref 0 in
   let racycl = ref 0 in
   let raspd = ref 0.0 in
   let rastyl = ref "" in
   let rastfrm = ref 0 in
   let rimg = ref "" in
   let rsticky = ref 0.0 in
   let rexplode = ref false in
   let rtimeout = ref false in
   let rexwhit = ref false in
   let rlen = ref 0.0 in
   let rbounce = ref 0.0 in
   let rdamage = ref 0.0 in
   let rdpulse = ref (-1.0) in
   let rttl = ref 0.0 in
   let raw = Uiopt_f.load_weapon_file filename in
   for count = 0 to (List.length raw - 1) do
      let (a, b) = List.nth raw count in
      match a with
       | "bullet_name" -> rbname := b
       | "launch_on_explosion" -> rcoe := b :: !rcoe
       | "launch_trail" -> rltrail := b :: !rltrail
       | "trail_delay" -> rtdelay := (float_of_string b)
       | "number_to_create" -> rnumcoe := (int_of_string b)
       | "scatter_pattern" -> rscatter := b
       | "gravity" -> rgrav := (float_of_string b)
       | "remove_on_wormhit" -> rdowh := (bool_of_string b)
       | "is_blood" -> rdeco := (bool_of_string b)
       | "die_out_of_bounds" -> rdoob := (bool_of_string b)
       | "width" -> rwdt := (int_of_string b)
       | "height" -> rhgt := (int_of_string b)
       | "anim_cycles" -> 
            let acycl = int_of_string b in 
               racycl := if acycl > 0 then (acycl - 1) else acycl
       | "anim_speed" -> raspd := (float_of_string b)
       | "anim_style" -> rastyl := b
       | "anim_start_frame" -> rastfrm := (int_of_string b)
       | "image" -> rimg := b
       | "sticky" -> rsticky := (float_of_string b)
       | "explodes_on_ground" -> rexplode := (bool_of_string b)
       | "explodes_on_timeout" -> rtimeout := (bool_of_string b)
       | "explodes_on_wormhit" -> rexwhit := (bool_of_string b)
       | "length" -> rlen := (float_of_string b)
       | "bounce" -> rbounce := (float_of_string b)
       | "damage" -> rdamage := (float_of_string b)
       | "damage_pulse" -> rdpulse := (float_of_string b)
       | "time_to_live" -> rttl := (float_of_string b)
       | _ -> ()
   done;
   let new_bullet =
   {
      pname = !rbname;
      launch_on_explosion = !rcoe;
      launch_trail = !rltrail;
      trail_delay = !rtdelay;
      gravity = !rgrav;
      die_on_wormhit = !rdowh;
      is_blood = !rdeco;
      die_out_of_bounds = !rdoob;
      width = !rwdt;
      height = !rhgt;
      anim_cycles = !racycl;
      anim_speed = !raspd;
      anim_style = !rastyl;
      anim_start_frame = !rastfrm;
      image = !rimg;
      rope = false;
      sticky = !rsticky;
      explodes_on_ground = !rexplode;
      explodes_on_timeout = !rtimeout;
      explodes_on_wormhit = !rexwhit;
      length = !rlen;
      bounce = !rbounce;
      damage = !rdamage;
      damage_pulse = !rdpulse;
      time_to_live = !rttl;
   }
   in (!rbname, new_bullet)
;;

let load_weaponfile filename bullets =
   let rname = ref "" in
   let rbullet = ref proto_bullet in
   let rsao = ref 0 in
   let rspr = ref 0 in
   let rsd = ref 0.0 in
   let rssprd = ref 0.0 in
   let rsspd = ref 0.0 in
   let rscatter = ref "" in
   let rrtime = ref 0.0 in
   let rkick = ref 0.0 in
   let raw = Uiopt_f.load_weapon_file filename in
   for count = 0 to (List.length raw - 1) do
      let (a, b) = List.nth raw count in
      match a with
       | "name" -> rname := b
       | "bullet" -> rbullet := (match_bullet b bullets)
       | "shots_at_once" -> rsao := (int_of_string b)
       | "shots_per_reload" -> rspr := (int_of_string b)
       | "shot_delay" -> rsd := (float_of_string b)
       | "shot_spread" -> rssprd := (float_of_string b)
       | "shot_speed" -> rsspd := (float_of_string b)
       | "scatter_pattern" -> rscatter := b
       | "reload_time" -> rrtime := (float_of_string b)
       | "kick" -> rkick := (float_of_string b)
       | _ -> ()
   done;
   let new_wpn =
   {
      wname = !rname;
      l_bullet = !rbullet;
      shots_at_once = !rsao;
      shots_per_reload = !rspr;
      shot_delay = !rsd;
      shot_spread = !rssprd;
      shot_speed = !rsspd;
      scatter = !rscatter;
      reload_time = !rrtime;
      kick = !rkick;
   }
   in (!rname, new_wpn)
;;

let load_set lf filename prefix bl =
   let rec bunch_weapons load_f l bullets =
      match l with
       | [] -> []
       | (a,  b) :: tail ->
            if (a = "load") then
               load_f (prefix ^ b) bullets 
                     :: bunch_weapons load_f tail bullets
            else bunch_weapons load_f tail bullets
   in
   let rec list_weapon_names l =
      match l with
         [] -> []
       | (a, b) :: tail ->
             a :: list_weapon_names tail
   in
   let raw = Uiopt_f.load_weapon_file (prefix ^ filename) in
      let weaponbunch = bunch_weapons lf raw bl in
      let weapon_names = list_weapon_names weaponbunch in
      weaponbunch, weapon_names
;;

let load_weaponset filename prefix bullets =
   load_set load_weaponfile filename prefix bullets
;;

let load_bulletset filename prefix =
   load_set load_bulletfile filename prefix []
;;

let new_shotgun () = 
   {l = shotgun; shots_left = shotgun.shots_per_reload; reloading = 0.0}
;;

let new_uzi () = 
   {l = uzi; shots_left = uzi.shots_per_reload; reloading = 0.0}
;;

let new_weapon w = 
   {l = w; shots_left = w.shots_per_reload; reloading = 0.0}
;;
