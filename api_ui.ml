(*
    Jude's User Interface
    Copyright (C) 2004-2010  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Provides a bunch of messy menu-rendering, question-asking, and 
other user interface functions wrapped up in an object.
*)


let rec count_back_lines menulist hilit limit =
   if hilit < 0 || limit < 1 then
      hilit + 1
   else count_back_lines menulist (hilit - 1) (limit - 
      (menulist#nth hilit)#get_numlines)
;;

let set_menulist_startline menulist wheight =
   (menulist#nth 0)#set_start_line 0;
   (menulist#nth 0)#set_page_start 0;
   (menulist#nth 0)#set_page 0;
   for count = 1 to ((List.length menulist#get_index) - 1) do
    (menulist#nth count)#set_start_line
       (((menulist#nth (count - 1))#get_start_line +
       (menulist#nth (count - 1))#get_numlines) + 1);
    if List.length menulist#get_index / (wheight / 2) = 0 then
      (menulist#nth count)#set_page 0
    else
      if (((menulist#nth count)#get_start_line / (wheight + 2))) <=
       (menulist#nth (count - 1))#get_page then
         (
          (menulist#nth count)#set_page 
           ((menulist#nth (count - 1))#get_page);
          (menulist#nth count)#set_page_start
           ((menulist#nth (count - 1))#get_page_start)
         )
      else
         (
          (menulist#nth count)#set_page 
           (((menulist#nth (count - 1))#get_page) + 1);
          (menulist#nth count)#set_page_start (count - 1)
(*          ((menulist#nth (count))#get_start_line)*)
         )
   done
;;

let set_menulist_linelength menulist cursor =
   for count = 0 to ((List.length menulist#get_index) - 1 ) do
      (menulist#nth count)#set_num_lines cursor#getboundx
   done
;;

let rec search_for_index (c:char) 
(menulist:Menulist.menulist) choice count =
   (if count = List.length menulist#get_index then -1
    else
    if (choice + 1) = (List.length menulist#get_index) then
       search_for_index c menulist (-1) (count + 1)
    else
       if (menulist#get_item(List.nth menulist#get_index 
          (choice + 1)))#get_itemtype = "option" then
             (if (List.nth menulist#get_index (choice + 1)) = c then
                 (choice + 1)
              else search_for_index c menulist (choice + 1) (count + 1)
             )
       else search_for_index c menulist (choice + 1) (count + 1)
   )
;;

let rec move_menu_choice_up (menulist:Menulist.menulist) choice =
   match (choice - 1) with
      -1 -> move_menu_choice_up menulist (List.length
         menulist#get_index)
    | n -> if (menulist#get_item (List.nth menulist#get_index n)
              )#get_itemtype =
                 "option" then n
         else move_menu_choice_up menulist n
;;

let rec move_menu_choice_down (menulist:Menulist.menulist) choice =
   if List.length menulist#get_index = 0 then 0 else
   if (choice + 1) = (List.length menulist#get_index) then
      move_menu_choice_down menulist (-1)
   else
      if (menulist#get_item(List.nth menulist#get_index 
         (choice + 1)))#get_itemtype = "option" then
            (choice + 1)
      else move_menu_choice_down menulist (choice + 1)
;;

let rec move_menu_screen_up (menulist:Menulist.menulist) choice =
   let orig = (menulist#nth choice)#get_page in
      real_move_menu_screen_up menulist choice 0 orig
and real_move_menu_screen_up menulist choice count original_screen =
   if count >= List.length (menulist#get_index) then 0
   else if choice < 0 then 
      real_move_menu_screen_up menulist 
                               (List.length menulist#get_index - 1)
                               (count + 1) original_screen
   else
   match (choice - 1) with
      -1 -> real_move_menu_screen_up menulist (List.length
         menulist#get_index) (count + 1) original_screen
    | n -> if not ((menulist#get_item (List.nth menulist#get_index n)
                   )#get_page = original_screen
                  ) then n
         else real_move_menu_screen_up menulist n (count + 1)
                                       original_screen
;;

let rec move_menu_screen_down (menulist:Menulist.menulist) choice =
   let orig = (menulist#nth choice)#get_page in
      real_move_menu_screen_down menulist choice 0 orig
and real_move_menu_screen_down menulist choice count original_screen =
   if count >= List.length (menulist#get_index) then 0 else
   if (choice + 1) >= (List.length menulist#get_index) then
      real_move_menu_screen_down menulist (-1) (count + 1) original_screen
   else
      if not ((menulist#get_item(List.nth menulist#get_index 
             (choice + 1)))#get_page = original_screen) then
               (choice + 1)
      else real_move_menu_screen_down menulist (choice + 1)
                                      (count + 1) original_screen
;;

let rec clear_bool_pixels bool_map pl =
   match pl with
      [] -> ()
    | (lx, ly) :: tail ->
         let row = Array.get bool_map lx in
         Array.set row ly (false, false, false)
;;

type gbox =
 {
   mutable x: int;
   mutable y: int;
   mutable w: int;
   mutable h: int;
 }
;;

class toplevel_ui option =
   object (self)
      val cset = Boxes.get_cursor_set option
      val mutable gb_set = []
      val base_ui = Uilink.new_ui option
      val option = option
      method initialise =
         base_ui#initialise
      method ask q = 
        (
         self#clear_win "msg" "arena";
         self#clear_win "msg" "arena";
         self#reset_cursor "msg";
         self#set_colour (U_colour.conv_colour_s "grey");
         self#box_ask q "msg"
        )
      method end_ui = base_ui#end_ui
      method colour_say thing colour =
         (
             self#set_colour colour;
             self#box_say thing "msg"
         )
      method colour_say_up thing colour =
         (
             self#set_colour colour;
             self#box_say_up thing "msg"
         )
      method say thing = 
         (
             self#set_colour (U_colour.conv_colour_s "grey");
             self#box_say thing "msg"
         )
      method set_colour colour = base_ui#set_colour colour
      method box_ask q cursorstring = 
         base_ui#box_ask q (cset#get_cursor cursorstring)
      method box_say_up thing cursorstring surface =
         base_ui#box_say_up thing (cset#get_cursor cursorstring) surface
      method box_say thing cursorstring surface =
         base_ui#box_say thing (cset#get_cursor cursorstring) surface
      method clear_win cursorstring= base_ui#clear_win
                                     (cset#get_cursor cursorstring)
      method clear_say thing target =
         (
            self#clear_win "msg" target;
            self#clear_win "msg" target;
            self#reset_cursor "msg" ;
            self#say thing target
         )
      method free_say strn x y target =
         base_ui#free_say strn x y target
      method free_say_retbox strn x y target =
         base_ui#free_say_retbox strn x y target
      method in_key = base_ui#in_key
      method reset_box boxstring off_x off_y boundx boundy =
         cset#changebox boxstring off_x off_y boundx boundy
      method display_scrolling_menu (menulist:Menulist.menulist)
       (hilit:int) (prev_hilit:int) height  =
        try
        (
         let count = ref 
            (menulist#nth hilit)#get_page_start
         in
         let lines_end = ref false in
         let index = menulist#get_index in
            while ( !count < ((List.length index)) )
             && !lines_end = false do
               (if !count > (-1) then
                  let prevcolour = (option#get_colour "c_colour") in
                  (if (!count) = hilit then
                     (self#set_colour (option#get_colour "menu_fc");
                      self#box_say " +  " "temp" "direct"
                     )
                   else
                     ( 
                      self#set_colour (option#get_colour "menu_bc");
                      if
                         ((menulist#nth (!count))#get_itemtype 
                          = "option")
                      then
                      self#box_say " -  " "temp" "direct"
                     )
                   ;
(*
                   self#box_say (String.make 1 
                         (List.nth index (!count))) "temp" "direct";
*)
                   if not 
                      ((menulist#nth (!count))#get_itemtype = "option")
                   then
                      self#set_colour (option#get_colour "menu_hc");
                   self#box_say (menulist#nth (!count))#get_content
                                "temp" "direct";
                   self#set_colour prevcolour
                  )
               );
               lines_end := self#scrollbox_newline_over;
               count := (!count + 1)
            done
         )
        with e -> raise (Failure ("display_scrolling_menu: " ^
              Printexc.to_string e))
      method scrollbox_newline_over =
         let tc = (cset#get_cursor "temp") in
            if tc#gety = (tc#getboundy -1 ) then true else
               (tc#newline ();
               if tc#gety = (tc#getboundy -1 ) then true else
                  (tc#newline ();
                  if tc#gety = (tc#getboundy -1) then true else false
                  )
               )
(*
      method display_menu (menulist:Menulist.menulist) (hilit:int)=
         let index = (menulist#get_index) in
            for count = 1 to List.length menulist#get_index do
               let prevcolour = option#get_c_colour in
                  (if (count-1) = hilit then
                     self#set_colour option#get_menu_fc;
                   self#box_say (String.make 1 
                         (List.nth index (count-1))) "temp" 
                         "direct";
                   if (menulist#nth (count - 1))#get_itemtype = "option"
                   then
                      self#box_say " )  " "temp"
                   else
                      (self#set_colour option#get_menu_hc;
                       self#box_say "> " "temp"
                      )
                   ;
                   self#box_say (menulist#nth (count-1))#get_content
                                "temp" ;
                   self#box_say "\n\n" "temp" ;
                   self#set_colour prevcolour
                  )
            done
*)
      method reset_cursor cursorstring =
         (cset#get_cursor cursorstring)#reset
(*
      method choose_scrolling_menuitem (uiopt:Ui_opt.option) menulist
            (height:int) 
      =
        try
        (
         let choice = ref (move_menu_choice_down menulist (-1)) in
         let prevchoice = ref !choice in
         let keypress = ref " " in
            set_menulist_linelength menulist (cset#get_cursor "temp");
            set_menulist_startline menulist (cset#get_cursor
                                            "temp")#getboundy;
            while not (!keypress = "enter") do
               ( self#clear_win "temp";
                (cset#get_cursor "temp")#reset;
                self#display_scrolling_menu menulist !choice
                   !prevchoice height;
                base_ui#sync_screen;
                prevchoice := !choice;
                keypress := base_ui#in_key;
                if !keypress = option#get_key_previous then
                   choice := move_menu_choice_up menulist !choice
                else if !keypress = option#get_key_next then
                   choice := move_menu_choice_down menulist !choice
                else if !keypress = "enter" then ()
                else if not (search_for_index !keypress.[0] 
                   menulist !choice 0 = (-1)) then
                     choice := search_for_index 
                        !keypress.[0] menulist !choice 0
                  else
                     (* () *) raise Not_found (* DEBUG *)
               ) (*self#say (string_of_int 
                  (menulist#nth !choice)#get_page)*)
            done;
         (menulist#nth_index !choice)
        )
        with e -> raise (Failure ("choose_scrolling_menuitem: " ^
              Printexc.to_string e))
*)
      method vectorb_choose_menuitem (uiopt:Ui_opt.option) menulist
            (height:int) 
      =
         let choice = ref (move_menu_choice_down menulist (-1)) in
         let prevchoice = ref !choice in
         let keyaction = ref Olevent.Nothing in
         let chosen = ref false in
            set_menulist_linelength menulist (cset#get_cursor "temp");
            set_menulist_startline menulist (cset#get_cursor
                                            "temp")#getboundy;
            while not (!chosen = true) do
               ( self#clear_win "temp" "direct";
                 self#clear_win "temp" "direct";
                (cset#get_cursor "temp")#reset;
                self#display_scrolling_menu menulist !choice
                   !prevchoice height;
                base_ui#sync_screen;
                prevchoice := !choice;
                keyaction := base_ui#get_menu_action;
                if !keyaction = Olevent.Up
                || !keyaction = Olevent.Right then
                   choice := move_menu_choice_up menulist !choice
                else if !keyaction = Olevent.Left
                || !keyaction = Olevent.Down then
                   choice := move_menu_choice_down menulist !choice
                else if !keyaction = Olevent.Fire then
                   chosen := true
               )
            done;
         (menulist#nth_index !choice)
(*
      method display_fast_menu (menulist:Menulist.menulist) hilit height 
      =
         let count = ref 
            (menulist#nth hilit)#get_page_start
         in
         let lines_end = ref false in
         let index = menulist#get_index in
            while ( !count < ((List.length index)) )
             && !lines_end = false do
               (if !count > (-1) then
                  let prevcolour = option#get_c_colour in
(* *)             (
(*
                  (if (!count) = hilit then
                     (self#set_colour option#get_menu_fc;
                      self#box_say "*-> " "temp" 
                     )
                   else
*)
                   self#set_colour option#get_menu_bc
                   ;
                   self#box_say (String.make 1 
                         (List.nth index (!count))) "temp" ;
                   if (menulist#nth (!count))#get_itemtype = "option"
                   then
                     (
                      if (menulist#nth (!count))#get_selected then
                         self#box_say " +  " "temp" 
                      else self#box_say " -  " "temp"
                     )
                   else
                      (self#set_colour option#get_menu_hc;
                       self#box_say "> " "temp"
                      )
                   ;
                   self#box_say (menulist#nth (!count))#get_content
                                "temp";
                   self#set_colour prevcolour
                  )
               );
               lines_end := self#scrollbox_newline_over;
               count := (!count + 1)
            done
*)
(*
      method choose_fast_menuitem (uiopt:Ui_opt.option) menulist
            (height:int) 
      =
         let choice = ref (move_menu_choice_down menulist (-1)) in
         let chosen = ref false in
         let keypress = ref " " in
            set_menulist_linelength menulist (cset#get_cursor "temp");
            set_menulist_startline menulist (cset#get_cursor
                                            "temp")#getboundy;
            while not !chosen do
               ( self#clear_win "temp";
                (cset#get_cursor "temp")#reset;
                self#display_fast_menu menulist !choice height;
                base_ui#sync_screen;
                keypress := base_ui#in_key;
                if !keypress = option#get_key_previous then
                   choice := move_menu_screen_up menulist !choice
                else if !keypress = option#get_key_next then
                   choice := move_menu_screen_down menulist !choice
                else if !keypress = "enter" then raise Not_found
                else if not (search_for_index !keypress.[0]
                      menulist 0 0 = (-1)) then
                    (
                     choice := search_for_index 
                        !keypress.[0] menulist 0 0;
                     chosen := true
                    )
                  else
                     raise Not_found
               )
            done;
         (menulist#nth_index !choice)
*)
      method choose_menuitem uiopt menulist height =
(*
            if uiopt#is_fast_menu then
               menulist#get_item
                  (self#choose_fast_menuitem uiopt menulist height)
            else
*)
               menulist#get_item
                  (self#vectorb_choose_menuitem uiopt menulist 
height)
      method choose_menuindex uiopt menulist height =
(*
         if uiopt#is_fast_menu then
            (self#choose_fast_menuitem uiopt menulist height)
         else
*)
            (self#vectorb_choose_menuitem uiopt menulist 
height)
      method choose_many_indices uiopt menulist height =
         let finished = ref false in
         while not !finished do
           try
            let selection =
(*
               if uiopt#is_fast_menu then
                  (self#choose_fast_menuitem uiopt menulist height)
               else
*)
                  (self#vectorb_choose_menuitem uiopt menulist height)
            in
               let item = (menulist#get_item selection) in
                  let prev = item#get_selected in
                     item#set_selected (not prev)
           with Not_found -> finished := true
         done;
         self#selected_indices menulist
      method selected_indices menulist =
         let rslt = ref [] in
         let index = menulist#get_index in
            for count = 0 to (List.length index - 1) do
               let cur_ind = (List.nth index count) in
                  let item = menulist#get_item cur_ind in
                     if item#get_selected then
                        rslt := !rslt @ [cur_ind]
            done;
            !rslt
      method arena_sync = 
         base_ui#display_arena;
         base_ui#sync_screen
      method display_120px_bar endname barname pc x y =
         self#display_graphic "master" endname x y "direct";
         if pc > 0 then
            let percentage = min 100 pc in
            let color = 
               match barname with "healthbar" ->
                  ( (((100 - percentage) * 255) / 100),
                   ((percentage * 255) / 100), 0, 64)
                | _ -> (128, 0, ((percentage * 255) / 100), 192 )
            in
            base_ui#fill_rect 
                  ((x + 10), (y + 2), (x + 10 + percentage),
                   (y + 8))
                  "direct" color;
(*
            self#display_partial_graphic "master" barname 
                  (x + 10) (y + 2) 
                  (10, 2, percentage, 6) "direct"
*)
      method dual_splitscreen_sync (x1, y1) (x2, y2) aw ah 
            (p1:Player.player) (p2:Player.player) 
      =
         let box = List.assoc "master" gb_set in
            let play_h = box.h - 42 in
            let halfx = box.w / 2 in
            let halfy = play_h / 2 in
            let fx1 = 
               min (aw - halfx) (max 0 (x1 - (halfx / 2))) 
            in
            let fy1 = 
               min (ah - play_h) (max 0 (y1 - halfy)) 
            in
            let fx2 = 
               min (aw - halfx) (max 0 (x2 - (halfx / 2))) 
            in
            let fy2 = 
               min (ah - play_h) (max 0 (y2 - halfy)) 
            in
               base_ui#arena_to_screen 
                     (fx1, fy1, halfx, play_h) (0, 0);
               base_ui#arena_to_screen 
                     (fx2, fy2, halfx, play_h) (halfx, 0);
               base_ui#fill_rect (0, play_h, box.w, box.h) "direct" 
                     (0,0,0,255);
               let p1h = int_of_float (p1#get_health) in
               let p2h = int_of_float (p2#get_health) in
               self#display_120px_bar "healthbar_ends" "healthbar" 
                     p1h 20 (box.h - 40);
               self#display_120px_bar "healthbar_ends" "healthbar" 
                     p2h (20 + halfx) (box.h - 40);
(* *)
               let p1r, endimg = 
                  if p1#get_reload <= 0.0 then
                     ((p1#get_shots_left * 100) / p1#get_total_shots)
                     , "reloaded_ends"
                  else
                     int_of_float 
                        (((p1#get_reload_from -. p1#get_reload) 
                        *. 100.) /. p1#get_reload_from)
                     , "reloading_ends"
               in
               let p2r, endimg2 = 
                  if p2#get_reload <= 0.0 then
                     ((p2#get_shots_left * 100) / p2#get_total_shots)
                     , "reloaded_ends"
                  else
                     int_of_float 
                        (((p2#get_reload_from -. p2#get_reload) 
                        *. 100.) /. p2#get_reload_from)  
                     , "reloading_ends"
(*
                  int_of_float (((p2#get_reload_from -. p2#get_reload) 
                  *. 100.) /. p2#get_reload_from)  
*)
               in
(* *)
(*               let p1r = 100 in let p2r = 100 in *)
               self#display_120px_bar endimg "reloadbar" 
                     p1r 20 (box.h - 20);
               self#display_120px_bar endimg2 "reloadbar" 
                     p2r (20 + halfx) (box.h - 20);
               base_ui#sync_screen;
      method sync = (* base_ui#sync_screen *) self#arena_sync
      method box_mimic_ret_height boxtarget boxsource =
         let boxt = cset#get_cursor boxtarget in
         let boxs = cset#get_cursor boxsource in
            boxt#complete_reset
               boxs#get_x_offset
               boxs#get_y_offset
               boxs#getboundx
               boxs#getboundy;
            boxs#getboundy
      method fullscreen boxtarget =
         let boxt = cset#get_cursor boxtarget in
            boxt#complete_reset 0 0 
               (option#get_int "screen_chars_wide")
               (option#get_int "screen_chars_high")
      method fullscreen_ret_h boxtarget =
         let boxt = cset#get_cursor boxtarget in
            boxt#complete_reset 0 0 
               (option#get_int "screen_chars_wide")
               (option#get_int "screen_chars_high");
         option#get_int "screen_chars_high"
      method line_up cursorstring =
         (cset#get_cursor cursorstring)#lineup ()
      method last_line cursorstring =
         (cset#get_cursor cursorstring)#lastline ()
      method load_image = base_ui#load_image
(*      method blit = base_ui#blit *)
      method display_image = base_ui#display_image
      method box_image (boxname:string) img x y surface = 
         try
            let thisbox = List.assoc boxname gb_set in
            let nx = min (thisbox.x + x) (thisbox.x + thisbox.w) in
            let ny = min (thisbox.y + y) (thisbox.y + thisbox.h) in
               base_ui#display_image img nx ny surface
         with Not_found -> print_string "invalid box\n"
      method show_keybindings (uiopt:Ui_opt.option) = ()
      method clean_dirty = base_ui#clean_dirty
      method load_graphic = base_ui#load_graphic
      method display_graphic boxname (gn:string) x y target =
         try
            let thisbox = List.assoc boxname gb_set in
            let nx = min (thisbox.x + x) (thisbox.x + thisbox.w) in
            let ny = min (thisbox.y + y) (thisbox.y + thisbox.h) in
               base_ui#display_graphic gn nx ny target
         with Not_found -> print_string "invalid box\n"
      method display_partial_graphic boxname (gn:string) x y 
            (subx, suby, width, height) target
      =
         try
            let thisbox = List.assoc boxname gb_set in
            let nx = min (thisbox.x + x) (thisbox.x + thisbox.w) in
            let ny = min (thisbox.y + y) (thisbox.y + thisbox.h) in
               base_ui#display_partial_graphic gn nx ny 
                     (subx, suby, width, height) target
         with Not_found -> print_string "invalid box\n"
      method blitover_graphic gn1 gn2 subbox x y =
         try
            base_ui#blitover_graphic gn1 gn2 subbox x y
         with Not_found -> print_string "error in blitover_graphic\n"
      method cover_old_sprite boxname cover_g with_g x y =
         try
            let thisbox = List.assoc boxname gb_set in
            let nx = min (thisbox.x + x) (thisbox.x + thisbox.w) in
            let ny = min (thisbox.y + y) (thisbox.y + thisbox.h) in
               base_ui#cover_sprite cover_g with_g nx ny "arena"
         with Not_found -> print_string "invalid box\n"
      method cover_box boxname with_g (lx, ly, wdt, hgt) =
         try
            let thisbox = List.assoc boxname gb_set in
            let nx = min (thisbox.x + lx) (thisbox.x + thisbox.w) in
            let ny = min (thisbox.y + ly) (thisbox.y + thisbox.h) in
               base_ui#cover_box with_g (nx, ny, wdt, hgt) "arena"
         with Not_found -> print_string "invalid box in cover_box\n"
      method begin_arena_mode = base_ui#begin_arena_mode
      method end_arena_mode = base_ui#end_arena_mode
      method get_actions = base_ui#get_actions
      method get_gdim = base_ui#get_gdim
      method rope_pixels pl =
(*
         if List.length pl > 0 then 
           (
*)
            let thisbox = List.assoc "levelbox" gb_set in
            base_ui#paint_alternate_colours "blah?FIXME" 
                  thisbox.x thisbox.y pl
                  (160, 80, 0) (200, 100, 0) "arena"
(*
           )
*)
      method remove_pixels gname pl =
        try
           base_ui#paint_pixels "static_bg" "background" "arena" pl;
        with e -> raise
           (Failure (gname ^ " failed in Api_ui.remove_pixels: " ^
            (Printexc.to_string e)))
      method get_pixel_rgb = base_ui#get_pixel_rgb
      method fill_map bool_map gname =
        try
         let mx, my = self#get_gdim gname in
(* debug - see what images are loaded and their sizes.
         print_string (gname ^ ": " ^ (string_of_int mx) 
               ^ "x" ^ (string_of_int my) ^ "\n");
*)
         for countx = 0 to (mx - 1) do
            let b_row = 
              try
               Array.get bool_map countx 
              with e -> raise (Failure "Array.get failed")
            in
            for county = 0 to (my - 1) do
               let r, g, b = base_ui#get_pixel_rgb countx county gname 
               in
                  let worm_collide = not (r = 0) in
                  let bullet_collide = not (g = 0) in
                  let destroyable = not (b = 0) in
                    try
                     Array.set b_row county 
                           (worm_collide, bullet_collide, destroyable)
                    with e -> raise 
                          (Failure "Array.set failed in fill_map")
            done
         done
        with e -> raise 
           (Failure (gname ^ " failed in Api_ui.fill_map:\n" ^
            (Printexc.to_string e)))
(*
      method debug_bool_map bool_map x y =
         for count_x = 0 to x do
            for count_y = 0 to y do
               let result = bool_map.(count_x).(count_y) in
               print_string 
                 (
                  if result then
                     ("1")
                  else
                     (" ")
                 )
            done;
            print_string "\n";
         done;
         print_string "\n";
*)
      method single_bool_map bool_map gname =
        try
         let mx, my = self#get_gdim gname in
(* debug - see what images are loaded and their sizes. *)
         print_string (gname ^ ": " ^ (string_of_int mx) 
               ^ "x" ^ (string_of_int my) ^ "\n");
(* *)
         for countx = 0 to (mx - 1) do
(*
            let b_row = 
              try
               Array.get bool_map countx 
              with e -> raise (Failure "Array.get failed")
            in
*)
            for county = 0 to (my - 1) do
               let r, g, b, a = 
                  base_ui#get_pixel_rgba countx county gname 
               in
                  let result = 
                        (not ((r = 0) && (g = 0) && (b = 0))) 
(*                     && (a > 0) *)
                  in
                     bool_map.(countx).(county) <- result;
(*
                    try
                     Array.set b_row county result
                    with e -> raise 
                          (Failure "Array.set failed in fill_map")
*)
            done
         done;
(*
         self#debug_bool_map bool_map (mx - 1) (my - 1);
*)
        with e -> raise 
           (Failure (gname ^ " failed in Api_ui.fill_map:\n" ^
            (Printexc.to_string e)))
      method make_bitmask gname =
         let dx, dy = self#get_gdim gname in
            let bmask = Array.make_matrix dx dy false in
               self#single_bool_map bmask gname;
               bmask
      method get_ticks = base_ui#get_ticks
      method delay = base_ui#delay
(*      method paint_pixel = base_ui#paint_pixel *)
      method clear_event_queue = base_ui#clear_event_queue
      method new_graphic_box gn lx ly width height =
         gb_set <- (gn, {x = lx; y = ly; w = width; h = height}) 
            :: gb_set
      method conditional_underline_graphic_rock gn = 
         base_ui#conditional_borderline_graphic gn 
            (255, 255, 0)
      method colour_material gn = 
         base_ui#fix_material_colours gn
      method create_composite newname under over =
         base_ui#create_composite newname under over;
      method set_alpha = base_ui#set_alpha
      method init_arena = base_ui#init_arena
   end
;;

let new_toplevel_ui option = new toplevel_ui option
