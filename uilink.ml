(*
    Jude's User Interface
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Provides the UI bridge

*)

let terminal_obj terminaltype option =
   match terminaltype with
      "" -> Ui_sdl.new_terminal option
    | _ -> Ui_sdl.new_terminal option
;;

(*
let terminal_obj terminaltype option =
   match terminaltype with
    | _ -> Ui_curs.new_terminal option 
;;
*)

(*
let terminal_obj terminaltype option =
   match terminaltype with
    | "sdl" -> Ui_sdl.new_terminal option
    | _ -> Ui_curs.new_terminal option 
;;
*)

class ui option =
   object (self)
      val interface = terminal_obj (option#get_key "terminaltype")
            option
      method initialise =
         interface#imp_setup_screen
      method end_ui = interface#end_ui
      method set_colour colour = interface#set_colour colour
      method box_ask q cursor = 
         interface#imp_ask q cursor
      method box_say thing cursor =
         interface#print_str thing cursor
      method box_say_up thing cursor =
         interface#print_str_up thing cursor
      method clear_win cursor = interface#clear_cursorwindow cursor
      method in_key = interface#in_key
      method sync_screen = interface#sync_screen
      method load_image = interface#load_image
(*      method blit = interface#blit ??? *)
      method display_image = interface#display_image
      method clean_dirty = interface#clean_dirty
      method load_graphic = interface#load_graphic
      method display_graphic = interface#display_graphic
      method display_partial_graphic = interface#display_partial_graphic
      method begin_arena_mode = interface#begin_arena_mode
      method end_arena_mode = interface#end_arena_mode
      method get_actions = interface#get_actions
      method get_gdim = interface#get_gdim
      method get_pixel_rgb = interface#get_pixel_rgb
      method get_pixel_rgba = interface#get_pixel_rgba
      method cover_sprite = interface#cover_sprite
      method cover_box = interface#cover_box
      method get_ticks = interface#get_ticks
      method delay = interface#delay
      method paint_pixel = interface#paint_pixel
      method clear_event_queue = interface#clear_event_queue
      method conditional_borderline_graphic = 
         interface#conditional_borderline_graphic
      method paint_pixels = interface#paint_surface_and_bg_pixels
      method create_composite = interface#create_composite
      method fix_material_colours = interface#fix_material_colours
      method get_menu_action = interface#get_menu_action
      method paint_alternate_colours = interface#paint_alternate_colours
      method display_arena = interface#display_arena
      method new_arena = interface#new_arena
      method arena_to_screen = interface#arena_to_screen
      method set_alpha = interface#set_alpha
      method fill_rect = interface#fill_rect
      method free_say = interface#free_say
      method free_say_retbox = interface#free_say_retbox
      method init_arena = interface#new_arena
      method blitover_graphic = interface#blitover_graphic
   end
;;


let new_ui option = new ui option;;
