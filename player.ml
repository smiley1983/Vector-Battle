(*
    Vector Battle
    Copyright (C) 2006 - 2010  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

*)

(*

*  What does this file do?

**    Provides the player object and supporting functions

*)

type status =
   Flying
 | Crawling
 | Wallhugging
 | Jumping
;;

type f_coord =
 {
   mutable x : float;
   mutable y : float
 }
;;

let process_action (event, state, pindex) pl =
   let keys = pl#get_keys in
   let (pnumber:int) = pl#get_pin in
      if pnumber = pindex then
         if (state && (not (event = Olevent.Quit))) then 
            (
             keys#press event; 
             true
            )
         else if (state && (event = Olevent.Quit)) then false
         else if event = Olevent.Jump then
            (
             pl#set_jumprelease true; 
             keys#release event; 
             true
            )
         else 
            (
             keys#release event; 
             true
            )
      else 
        (
         true
        )
;;

let rec step_through_actions actionl player =
   next_action actionl player true
and next_action actionl player contin =
   match actionl with
      [] -> contin
    | head :: tail ->
         let stillcontinue = process_action head player in
            next_action tail player (contin && stillcontinue)
;;

let rec backstep bool_map sbitm locx locy width height points =
   match points with
      [] -> locx, locy
    | (nlx, nly) :: tail ->
         let _, _, does_collide =
            Collide.wall_collision Collide.wall_maskget bool_map
               sbitm nlx nly (0, 0, width, height) true
         in
            if not does_collide then (nlx, nly)
            else backstep bool_map sbitm nlx nly width height tail
;;

let rec rope_force player rope elapsed =
   match rope with
      [] -> (0.0, 0.0)
    | head :: tail ->
        if head#get_sticky = 1. then
        (
         let rlx = head#internal_get_locx in
         let rly = head#internal_get_locy in
         let iplx, iply = player#get_sprite_centre in
         let plx, ply = 
            ((float_of_int iplx) -. 0.5),
            ((float_of_int iply) ) 
         in
            let dist = ( (rlx -. plx) *. (rlx -. plx) )
                  +. ( (rly -. ply) *. (rly -. ply) )
            in
            let length = head#get_length in
            let force = head#get_bounce /. 4000.0
            in
            if dist <= (length *. length) then
               next_rope player tail elapsed 0.0 0.0
            else
               let pveloc = player#get_velocity in
               let next_dist = 
                  ( (rlx -. (plx +. pveloc.x)) *. 
                     (rlx -. (plx +. pveloc.x)) )
                  +. ( (rly -. (ply +. pveloc.y)) 
                     *. (rly -. (ply +. pveloc.y)) )
               in
               let stretch = 
                  max 0.0
                  (min 1.0 ((dist -. (length *. length)) 
                     /. (length *. length) ) )
               in
               let multiplier = 
                  if (next_dist > dist) && (stretch = 1.0) then 4.0 
                  else 1.0 
               in
               let dx = rlx -. plx in
               let dy = rly -. ply in
               let signx = if dx < 0. then -1. else 1. in
               let signy = if dy < 0. then -1. else 1. in
               let scalex, scaley =
                  if (abs_float dx) > (abs_float dy) then
                     (1.0 *. signx), ((abs_float (dy /. dx)) *. signy)
                  else
                     ((abs_float (dx /. dy)) *. signx), (1.0 *. signy)
               in
                  next_rope player tail elapsed
                     (multiplier *. force *. elapsed *. stretch *. scalex)
                     (multiplier *. force *. elapsed *. stretch *. scaley)
        )
       else
                  next_rope player tail elapsed 0.0 0.0
and next_rope player rope elapsed rx ry =
   let nx, ny = rope_force player rope elapsed in
      (rx +. nx), (ry +. ny)
;;

class player gname gmaskn lx ly pin =
   object (self)
      val mutable graphic = (gname:string)
      val loc = ({x = lx; y = ly}:f_coord)
      val prev_loc = ({x = 0.0; y = 0.0}:f_coord)
      val velocity = ({x = 0.0; y = 0.0}:f_coord)
      val sprite_loc = ({x = 0.0; y = 0.0}:f_coord)
      val mutable push_speed = (1. /. 6.)
      val mutable keys_in_effect = 
         (Olevent.new_event_table ():Olevent.event_table);
      val mutable cstatus = Crawling
      val mutable health = 100.0
      val mutable jumpvel = 0.0
      val mutable jumprelease = true
      val mutable aim_dir = 90.0
      val mutable prev_aim_dir = 0.0
      val mutable sprite_aim_dir = 0.0
      val mutable crawl_cycle = 0.0
      val mutable facing = 0
      val mutable prev_facing = 0
(*
      val mutable reload = 0.0 
      val mutable reload_from = 100.0
*)
      val mutable fire_space = 0.0
      val mutable change_space = 0.0
(*      val mutable shots_left = 1 *)
      val mutable rope_reload = 0.0
      val mutable rope_release = true
      val mutable pin = pin
      val mutable img_width = 0
      val mutable img_height = 0
      val mutable supported = false
      val mutable try_climbing = false
      val mutable continue_wallhug = false
      val mutable digrelease = true
      val mutable wall_bitmask = "wallbar"
      val mutable dig_img = "9pinvis"
      val mutable weapons = []
      val mutable weapon_selected = 0
      val mutable display_selected_weapon = false;
      method init gname gmaskn dign x y id =
(*
         weapons <- [Weapon.new_uzi (); Weapon.new_shotgun ()];
*)
(*
         weapons <- [Weapon.uzi; Weapon.shotgun];
*)
         graphic <- gname;
         wall_bitmask <- gmaskn;
         dig_img <- dign;
         loc.x <- x;
         loc.y <- y;
         pin <- id;
      method get_pin = pin
      method set_graphic gname = graphic <- gname
      method get_aim = aim_dir
      method get_aim_dir = 
         match facing with
            0 -> -.aim_dir
          | _ -> aim_dir
      method get_prev_aim_dir = 
         match prev_facing with
            0 -> -.prev_aim_dir
          | _ -> prev_aim_dir
      method set_jumpvel n = jumpvel <- n
      method set_jumprelease n = jumprelease <- n
      method get_graphic = graphic
      method get_facing = facing
      method get_locx = loc.x
      method get_locy = loc.y
      method get_velocity = velocity
      method get_veloc_x = velocity.x
      method get_veloc_y = velocity.y
      method get_display_selected_weapon = display_selected_weapon
      method get_weapon = List.nth weapons weapon_selected
      method int_locx = int_of_float (loc.x +. 0.5)
      method int_locy = int_of_float (loc.y +. 0.5)
      method get_int_loc = self#int_locx, self#int_locy
      method centre_loc = 
         (loc.x +. ((float_of_int img_width ) /. 2.) ),
         (loc.y +. ((float_of_int img_height) /. 2.) )
      method spawn_loc x y =
         self#set_locx x;
         self#set_locy y;
         self#set_locx x;
         self#set_locy y;
      method set_locx n = 
         prev_loc.x <- (loc.x);
         loc.x <- n
      method set_locy n = 
         prev_loc.y <- loc.y;
         loc.y <- n
      method get_keys = keys_in_effect
      method get_reload = Weapon.get_reloading_time self#get_weapon
      method get_reload_from = Weapon.get_reload_from self#get_weapon
      method get_shots_left = Weapon.get_shots_left self#get_weapon
      method get_total_shots = 
         Weapon.get_shots_per_reload self#get_weapon
      method get_wall_bitmask = wall_bitmask
      method get_dig_img = dig_img
      method respond actionlist = 
         step_through_actions actionlist self
      method events_take_effect 
(*
            (bulletset: (string * Weapon.bullet_specs) list)
*)
            (weaponset: (string * Weapon.launcher) list)
            master_bitmask
            (bool_map: (bool * bool * bool) array array) xbound ybound
            gravity (rope: Weapon.projectile list ref) elapsed
      =
        try(
         let player_bitmask = List.assoc wall_bitmask master_bitmask in
         try_climbing <- false;
         continue_wallhug <- false;
         let weapon = self#get_weapon in
         let reload = Weapon.get_reloading_time weapon in
         let int_locx = self#int_locx in
         let int_locy = self#int_locy in
         let shot_result = ref ([]: Weapon.projectile list) in
         let rope_result = ref ([]: Weapon.projectile list) in
         let unhooked_result = ref ([]: Weapon.projectile list) in
         let myropes = Weapon.find_duplicate pin !rope in
         if reload > 0.0 then 
            Weapon.set_reload weapon (max 0.0 (reload -. elapsed));
         if fire_space > 0.0 then 
            fire_space <- (max 0.0 (fire_space -. elapsed));
         if change_space > 0.0 then 
            change_space <- (max 0.0 (change_space -. elapsed));
         prev_aim_dir <- aim_dir;
         if health < 0.0 then
           (
            health <- 100.0;
            let pcx, pcy = self#centre_loc in
            let new_exps = 
               (["bloodexplosion1"; "deathexplosion1"],
                     pcx, pcy, 0., 0., self#get_pin)
               :: (self#spawn bool_map xbound ybound master_bitmask)
            in
            shot_result := !shot_result @
               Weapon.make_new_particles_mk2 weaponset new_exps;
           );
         prev_loc.x <- loc.x;
         prev_loc.y <- loc.y;
         rope_reload <- rope_reload -. elapsed;
         let rf_x, rf_y = rope_force self myropes elapsed in
            velocity.x <- velocity.x +. rf_x;
            velocity.y <- velocity.y +. rf_y;
         if keys_in_effect#includes Olevent.Up then
           (
            aim_dir <- max 0.0 (aim_dir -. (elapsed *. 2.5))
           );
         if keys_in_effect#includes Olevent.Down then
           (
            aim_dir <- min 164.0 (aim_dir +. (elapsed *. 2.5))
           );
         if keys_in_effect#includes Olevent.Jump 
         && (not (keys_in_effect#includes Olevent.Change))
         && rope_release then
            (
                unhooked_result := myropes;
                if ((cstatus = Crawling) || supported) 
                && jumprelease && (not (cstatus = Wallhugging)) then
                  (
                   jumprelease <- false;
                   velocity.y <- (velocity.y -. 0.5);
                   cstatus <- Jumping;
                   jumpvel <- 10.0;
                  )
                else if (cstatus = Jumping) && (jumpvel > 0.0)
                && (not (rope_release = false)) then
                  (
                   velocity.y <- 
                         (velocity.y -. (0.6 *. (gravity *. elapsed)));
                   jumpvel <- (jumpvel -. elapsed)
                  )
            );
         if (keys_in_effect#includes Olevent.Change) && 
            (keys_in_effect#includes Olevent.Jump) 
         then
            if rope_reload <= 0.0 then 
            (
               rope_reload <- 20.0;
               rope_release <- false;
               let centre_x = float_of_int ((img_width + 1) / 2) in
               let centre_y = float_of_int ((img_height + 1) / 2) in
               let px, py = Algo.gun_line self#get_aim_dir 9.0 in
               let out_x = centre_x +. px +. loc.x in
               let out_y = centre_y +. py +. loc.y in
               let newrope =
                  Weapon.new_rope
                   self#get_aim_dir out_x out_y 8.0
                   velocity.x velocity.y pin
               in
               if (newrope#in_bounds xbound ybound) then 
                  rope_result := !rope_result @ [newrope]
            );
         if (keys_in_effect#includes Olevent.Change) then
            display_selected_weapon <- true
         else display_selected_weapon <- false;
         if keys_in_effect#includes Olevent.Change
         && (change_space <= 0.0)
         && keys_in_effect#includes Olevent.Left then
            (
               let numweaps = List.length weapons - 1 in
               weapon_selected <- 
                  if weapon_selected > 0 then 
                     (weapon_selected - 1)
                  else numweaps;
               change_space <- 5.0;
            );
         if keys_in_effect#includes Olevent.Change
         && (change_space <= 0.0)
         && keys_in_effect#includes Olevent.Right then
            (
               let numweaps = List.length weapons - 1 in
               weapon_selected <- 
                  if weapon_selected = numweaps then 0
                  else weapon_selected + 1;
               change_space <- 5.0
            );
         if (not (keys_in_effect#includes Olevent.Change)) && 
            (not (keys_in_effect#includes Olevent.Jump) )
         then
            rope_release <- true;
         if (not (keys_in_effect#includes Olevent.Left)) && 
            (not (keys_in_effect#includes Olevent.Right))
         then
            crawl_cycle <- 0.0;
         if keys_in_effect#includes Olevent.Fire 
         && (reload <= 0.0) && (fire_space <= 0.0) 
         && (self#get_shots_left > 0) then
           (
            let weapon = List.nth weapons weapon_selected in
            Weapon.deplete_ammo weapon;
            fire_space <- Weapon.get_shot_delay weapon;
            if Weapon.get_shots_left weapon = 0 then
              (
               Weapon.reload weapon;
              );
            let centre_x = float_of_int ((img_width + 1) / 2) in
            let centre_y = float_of_int ((img_height + 1) / 2) in
            let px, py = 
                  Algo.gun_line self#get_aim_dir 
                        (float_of_int img_width)
            in
            let out_x = centre_x +. px +. loc.x in
            let out_y = centre_y +. py +. loc.y in
            shot_result := !shot_result @ 
               Weapon.fire_launcher weapon
                self#get_aim_dir out_x out_y
                velocity.x velocity.y pin;
            let kickx, kicky = 
                  Algo.gun_line (self#get_aim_dir +. 180.)
                  (Weapon.get_kick weapon)
            in
            velocity.x <- velocity.x +. kickx;
            velocity.y <- velocity.y +. kicky;
           );
         if (keys_in_effect#includes Olevent.Right)
         && (keys_in_effect#includes Olevent.Left) then
           (
            if digrelease then
              (
               digrelease <- false;
               let px, py = Algo.gun_line self#get_aim_dir 3.5 in
               let px2, py2 = Algo.gun_line self#get_aim_dir 1.5 in
               let ox = (float_of_int img_width) /. 2. in
               let oy = (float_of_int img_height) /. 2. in
               let new_exps = 
                 [
                  ([dig_img], 
                     (loc.x +. ox +. px), (loc.y +. oy +. py), 0., 0.,
                     pin);
                  ([dig_img], (loc.x +. ox +. px2), 
                     (loc.y +. oy +. py2), 0., 0., pin);
                 ] 
               in
               shot_result := !shot_result 
                     @ Weapon.make_new_particles_mk2 weaponset new_exps;
               (!shot_result, !rope_result, !unhooked_result)
              )
            else (!shot_result, !rope_result, !unhooked_result)
           )
         else
           (
            digrelease <- true;
            if keys_in_effect#includes Olevent.Right 
            && not (keys_in_effect#includes Olevent.Change)
            then
              (
               prev_facing <- facing;
               facing <- 0;
               self#lateral_move 1 elapsed bool_map player_bitmask 
                     int_locx int_locy;
(*
               crawl_cycle <- crawl_cycle +. (elapsed /. 10.0);
               if crawl_cycle >= 4.0 then 
                  crawl_cycle <- crawl_cycle -. 4.0;
               let _, points, blocked =
                  Collide.wall_collision Collide.wall_maskget bool_map
                        player_bitmask (int_locx + 1) int_locy 
                        (0, 0, img_width, img_height) true
               in
               let _, min_y, _, max_y = Algo.minima_maxima points in
               print_string ("miny:" ^ (string_of_int min_y) ^ "\n");
               if not blocked then
                 (
                  let pspeed = if cstatus = Crawling then
                     (push_speed *. 1.0 (* 2.0 *) ) else push_speed
                  in
                  if velocity.x < pspeed then
                  velocity.x <- 
                     min pspeed 
                           (velocity.x +. (elapsed *. pspeed))
                 )
               else
                 (
(*
                  if ((img_height - min_y) < (img_height - 2)) 
*)
                  if min_y < 2
                  && (max_y >= (img_height - 2)) then
                     try_climbing <- true
                  else if max_y >= (img_height - 2) then 
                    (
                     velocity.y <- max 0.0 (velocity.y -. elapsed);
                     continue_wallhug <- true;
                     cstatus <- Wallhugging
                    )
                 )
*)
              );
            if keys_in_effect#includes Olevent.Left
            && not (keys_in_effect#includes Olevent.Change)
            then
              (
               prev_facing <- facing;
               facing <- 1;
               self#lateral_move (-1) elapsed bool_map player_bitmask 
                     int_locx int_locy;
(*
               crawl_cycle <- crawl_cycle +. (elapsed /. 10.0);
               if crawl_cycle >= 4.0 then 
                  crawl_cycle <- crawl_cycle -. 4.0;
               let _, points, blocked =
                  Collide.wall_collision Collide.wall_maskget bool_map
                        player_bitmask (int_locx - 1) int_locy 
                        (0, 0, img_width, img_height) true
               in
               let _, min_y, _, max_y = Algo.minima_maxima points in
               if not blocked then
                 (
                  let pspeed = if cstatus = Crawling then
                     (push_speed *. 1.0 (* 2.0*) ) else push_speed
                  in
                  if velocity.x > -.pspeed then
                  velocity.x <- 
                     max (-.pspeed) 
                           (velocity.x -. pspeed) 
                 )
               else
                 (
                  if ((img_height - min_y) < (img_height - 2)) 
                  && (max_y >= (img_height - 2)) then
                     try_climbing <- true
                  else if max_y >= (img_height - 2) then 
                    (
                     velocity.y <- max 0.0 (velocity.y -. elapsed);
                     continue_wallhug <- true;
                     cstatus <- Wallhugging
                    )
                 )
*)
              )
            ; (!shot_result, !rope_result, !unhooked_result)
           )
        )with n ->
           raise (Failure ("in Player.events_take_effect " 
                 ^ Printexc.to_string n))
      method lateral_move dir elapsed bool_map player_bitmask 
            int_locx int_locy 
      =
           crawl_cycle <- crawl_cycle +. (elapsed /. 10.0);
            if crawl_cycle >= 4.0 then 
               crawl_cycle <- crawl_cycle -. 4.0;
            let (offx, offy), points, blocked =
               Collide.wall_collision Collide.wall_maskget bool_map
                     player_bitmask (int_locx + dir) int_locy 
                     (0, 0, img_width, img_height) true
            in
            let _, pmin_y, _, pmax_y = Algo.minima_maxima points in
            let min_y, max_y = (pmin_y - offy), (pmax_y - offy) in
            if not blocked then
              (
               let pspeed = if cstatus = Crawling then
                  (push_speed *. 1.0 (* 2.0*) ) else push_speed
               in
               if dir = -1 then
                 (
                  if velocity.x > -.pspeed then
                  velocity.x <- 
                     max (-.pspeed) 
                           (velocity.x -. (elapsed *. pspeed)) 
                 )
               else
                 (
                  if velocity.x < pspeed then
                  velocity.x <- 
                     min pspeed 
                           (velocity.x +. (elapsed *. pspeed))
                 )
              )
            else
              (
               if ((img_height - min_y) < (img_height - 2)) 
               && (max_y >= (img_height - 2)) then
                  try_climbing <- true
               else if max_y >= (img_height - 2) then 
                 (
                  velocity.y <- max 0.0 (velocity.y -. elapsed);
                  continue_wallhug <- true;
                  cstatus <- Wallhugging
                 )
              )
      method gravity_pull n elapsed =
         if ((cstatus = Flying) || (cstatus = Jumping))
         && (not supported) then
            velocity.y <- velocity.y +. (n *. elapsed)
      method drag (elapsed:float) = ()
(*
         velocity.x <- velocity.x -. (velocity.x *. elapsed *. 0.01);
         velocity.y <- velocity.y -. (velocity.y *. elapsed *. 0.01);
*)
      method detect_collision 
         (bool_map: (bool * bool * bool) array array)
         (player_bitmask: bool array array) width height 
         elapsed
      =
            let int_locx, int_locy = 
               (int_of_float (loc.x +. 0.5)), 
               (int_of_float (loc.y +. 0.5))
            in
(*
            let (*max_x, max_y, min_x, min_y,*) points, does_collide = 
*)
            let _, points, does_collide = 
               Collide.wall_collision Collide.wall_maskget bool_map
                  player_bitmask int_locx int_locy 
                  (0, 0, img_width, img_height) true
            in
(* see if we're flying or climbing *)
               if not does_collide
               then
                 (
                  let _, _, support_now =
                     Collide.wall_collision Collide.wall_maskget 
                        bool_map player_bitmask int_locx (int_locy + 1) 
                        (0, 0, img_width, img_height) true
                  in
                  supported <- support_now;
                  if 
                     ((cstatus = Crawling) && (not supported))
                  || ((cstatus = Wallhugging) && not continue_wallhug)
                  then
                     cstatus <- Flying;
                  if (cstatus = Crawling) then
                    (
                     velocity.x <- velocity.x -. 
                           (elapsed *. 0.9 *. velocity.x);
                    );
                  if try_climbing then 
                    (
                     let _, _, climb_blocked =
                        Collide.wall_collision Collide.wall_maskget 
                           bool_map player_bitmask 
                           int_locx (int_locy - 1) 
                           (0, 0, img_width, img_height) true
                     in
                     if (not climb_blocked) 
                     && (not (cstatus = Jumping)) then 
                       (
                        velocity.y <- 0.0;
                        loc.y <- loc.y -. elapsed
                       );
                    )
                 )
(* crawl... *)
(*last resort, step back to the last position*)
               else
                 (
                  if not (cstatus = Wallhugging) then
                     cstatus <- Crawling;
                  let points =  Algo.bresenham_line int_locx int_locy 
                        (int_of_float (prev_loc.x +. 0.5)) 
                        (int_of_float (prev_loc.y +. 0.5)) 
                  in
                  let new_locx, new_locy = 
                        backstep bool_map player_bitmask 
                              int_locx int_locy img_width img_height 
                              points
                  in
                    (
                     loc.x <- (float_of_int new_locx); 
                     loc.y <- (float_of_int new_locy);
                     velocity.x <- 0.0;
                     velocity.y <- 0.0;
                    )
                 )
      method confirm_pos = self#set_locx loc.x; self#set_locy loc.y;
         sprite_loc.x <- loc.x;
         sprite_loc.y <- loc.y;
         sprite_aim_dir <- self#get_aim_dir
      method update_pos bool_map master_bitmask max_x max_y
         elapsed
      =
        try(
         let sbitmask = List.assoc wall_bitmask master_bitmask 
         in
         let width = (Array.length sbitmask) in
         let sample_col = Array.get sbitmask 0 in
         let height = (Array.length sample_col) in
         let mx = max_x - width in
         let my = max_y - height in
(* Euler integrator meets my needs for now *)
         loc.x <- (loc.x +. (velocity.x *. elapsed));
         loc.y <- (loc.y +. (velocity.y *. elapsed));
(* some bounds checking: *)
         if loc.x < 0.0 then 
            (loc.x <- 0.0; velocity.x <- 0.0);
         if loc.y < 0.0 then 
            (loc.y <- 0.0; velocity.y <- 0.0);
         if loc.x > (float_of_int mx) then 
            (loc.x <- (float_of_int mx); velocity.x <- 0.0);
         if loc.y > (float_of_int my) then 
            (loc.y <- (float_of_int my); velocity.y <- 0.0);
(* detect and resolve wall collisions, if any *)
         self#detect_collision bool_map sbitmask width height
            elapsed;
        )with n ->
           raise (Failure ("in Player.update_pos " 
                 ^ Printexc.to_string n))
      method get_sprite_locx = sprite_loc.x
      method get_sprite_locy = sprite_loc.y
      method int_sprite_locx = int_of_float (sprite_loc.x +. 0.5)
      method int_sprite_locy = int_of_float (sprite_loc.y +. 0.5)
      method get_sprite_centre =
         (self#int_sprite_locx + ((img_width + 1) / 2)),
         (self#int_sprite_locy + ((img_height + 1) / 2))
      method get_sprite_aim_dir = sprite_aim_dir
      method set_img_height n = img_height <- n
      method set_img_width n = img_width <- n
      method get_dimensions = img_width, img_height
      method get_img_rect = 
            self#int_locx, self#int_locy, img_width, img_height
      method get_crawl_cycle = int_of_float crawl_cycle
      method get_img_offset = 
         let x_graphic = (self#get_crawl_cycle * img_width) +
            match self#get_facing with
               0 -> 0 (* 1 FIXME *)
             | _ -> 2 + (4 * img_width)
         in
         let y_graphic =
            ((int_of_float ((self#get_aim /. 22.5) +. 0.5)) 
              * img_height) (* LOOKAGAIN *)
         in
         x_graphic, y_graphic
      method get_health = health
      method take_damage n = health <- (health -. n)
      method take_hit (projectile:Weapon.projectile) =
         let damage = projectile#get_damage in
             if damage <= 0.0 then [] else
               (
                self#take_damage damage;
                let cx, cy = self#get_sprite_centre in
                let fx, fy = float_of_int cx, float_of_int cy in
                let divf = (Random.float 9.) +. 1. in
(* *)
                let particles = 
                   if damage >= 1. then 
                      int_of_float (damage +. 0.6)
                else if (Random.float 1.) <= damage then 1
                else 0
                in
(* *)
(*
                let particles = 2 in
*)
                Weapon.fire_launcher 
                      (Weapon.bloodgun_with_shots (particles * 8))
                      self#get_aim_dir fx fy 
                      ((projectile#get_veloc_x /. divf) +. velocity.x) 
                      ((projectile#get_veloc_y /. divf) +. velocity.y) 
                      pin
               )
      method add_weapon w = weapons <- w :: weapons
      method spawn
         (bool_map: (bool * bool * bool) array array) xbound ybound
         master_bitmask 
      =
         let rec next_spawn_spot x y count =
           if count = 0 then (x, y) else
            let player_bitmask = 
               List.assoc wall_bitmask master_bitmask 
            in
            let _, _, does_collide =
               Collide.wall_collision Collide.wall_maskget bool_map
                     player_bitmask x y
                     (0, 0, img_width, img_height) true
            in
            if does_collide then
               next_spawn_spot ( Random.int xbound) (Random.int ybound)
                     (count - 1)
            else (x, y)
         in
         let px, py = next_spawn_spot (Random.int xbound) 
               (Random.int ybound) 10000
         in
         self#spawn_loc (float_of_int px) (float_of_int py);
         let pcx, pcy = self#centre_loc in
         [([self#get_dig_img], pcx, pcy, 0., 0.,
               self#get_pin)]
   end
;;

let new_player gname gmaskn lx ly = new player gname gmaskn lx ly;;

(*
let new_pixelstepper gname lx ly = new pixelstepper gname lx ly;;
*)

let rec fastest l =
   next_fproj l 0.0
and next_fproj l n =
   match l with
      [] -> n
    | head :: tail ->
         let vx, vy = head#get_veloc_x, head#get_veloc_y in
         let v = (vx *. vx) +. (vy *. vy) in
            let m = max v n in
               next_fproj tail m
;;

