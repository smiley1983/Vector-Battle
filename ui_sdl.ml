(*
    Jude's User Interface
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Wraps ocamlsdl for uilink, and provides some additional functions 
for interpreting keypresses. Actually, it does a bit more than that now.

*)

let uppercase opt c =
   match c with
    | '`' -> (opt#get_key "shift_bq").[0]
    | '1' -> (opt#get_key "shift_1").[0]
    | '2' -> (opt#get_key "shift_2").[0]
    | '3' -> (opt#get_key "shift_3").[0]
    | '4' -> (opt#get_key "shift_4").[0]
    | '5' -> (opt#get_key "shift_5").[0]
    | '6' -> (opt#get_key "shift_6").[0]
    | '7' -> (opt#get_key "shift_7").[0]
    | '8' -> (opt#get_key "shift_8").[0]
    | '9' -> (opt#get_key "shift_9").[0]
    | '0' -> (opt#get_key "shift_0").[0]
    | '-' -> (opt#get_key "shift_minus").[0]
    | '=' -> (opt#get_key "shift_equals").[0]
    | '\\' -> (opt#get_key "shift_bs").[0]
    | '[' -> (opt#get_key "shift_lsb").[0]
    | ']' -> (opt#get_key "shift_rsb").[0]
    | ';' -> (opt#get_key "shift_sc").[0]
    | '\'' -> (opt#get_key "shift_ap").[0]
    | ',' -> (opt#get_key "shift_com").[0]
    | '.' -> (opt#get_key "shift_dot").[0]
    | '/' -> (opt#get_key "shift_fs").[0]
    | _ -> Char.uppercase c
;;

let make_rect (x, y, x2, y2) =
   let w = x2 - x in
   let h = y2 - y in
      Sdlvideo.rect ~x:x ~y:y ~w:w ~h:h
;;

let char_of_key opt { Sdlevent.ke_state = state; 
                  Sdlevent.keysym = key ;
                  Sdlevent.keymod = kmod ;
                  Sdlevent.keycode = kcode } 
=
   let c = 
      try Sdlkey.char_of_key key
      with Invalid_argument _ -> Char.chr 127 
   in
      if kmod land Sdlkey.kmod_shift <> 0 then
         uppercase opt c
      else c
;;

let int_of_key opt 
              { Sdlevent.ke_state = state; 
		Sdlevent.keysym = key ;
		Sdlevent.keymod = kmod ;
		Sdlevent.keycode = kcode } 
=
  let i = 
    try (Sdlkey.int_of_key key)
    with Invalid_argument _ -> -1
   in
      if kmod land Sdlkey.kmod_shift <> 0 then
         Char.code (uppercase opt (Char.chr i))
      else i
;;

let sdl_init opts =
   Sdl.init ~auto_clean:true [ `VIDEO ] ;
   let lst =
      if opts#get_bool "fullscreen" then
         [ `SWSURFACE ; `DOUBLEBUF; `FULLSCREEN ]
      else
         [ `SWSURFACE ; `DOUBLEBUF ]
   in
   let dimx, dimy =
      let px = opts#get_int "screen_width" in
      let py = opts#get_int "screen_height" in
      let expand_x = max 1 (opts#get_int "expand_x") in
      let expand_y = max 1 (opts#get_int "expand_y") in
      let mx, my = px * expand_x, py * expand_y in
         match mx, my with
          | 320, 240 -> 320, 240
          | 320, 200 -> 320, 200
          | _ -> 640, 480
   in
   Sdlmouse.show_cursor false;
   Sdlvideo.set_video_mode dimx dimy lst
;;

let rec get_keydown opt =
    let evt = Sdlevent.wait_event () in
	match evt with
         | Sdlevent.KEYDOWN ke ->
              int_of_key opt ke
         | _ -> get_keydown opt
;;

let read_key opt =
   get_keydown opt
;;

let rec step_through_key_events key_events ks pindex =
   match key_events with
      [] -> []
    | (ke, status) :: tail ->
         if ks#contains ke then
            ((ks#rev_key ke), status, pindex)
               :: step_through_key_events tail ks pindex
         else step_through_key_events tail ks pindex
;;

let rec match_keys_with_keysets key_events action_keyset =
   next_keyset_match key_events action_keyset 0
and next_keyset_match key_events action_keyset count =
   match action_keyset with
      [] -> []
    | head :: tail ->
         step_through_key_events key_events head count
            @ next_keyset_match key_events tail (count + 1)
;;

let rec get_first_action key_events ks =
   match key_events with
      [] -> Olevent.Nothing
    | (ke, status) :: tail ->
         if (ks#contains ke) && status then
            (ks#rev_key ke)
         else get_first_action tail ks
;;

let rec collect_action_list ui events_happened =
      next_in_action_list ui events_happened [] [] 
and next_in_action_list ui event symlist result =
   match event with
      [] -> result
    | Sdlevent.KEYDOWN
      { 
        Sdlevent.ke_which = k_which;
        Sdlevent.ke_state = state; 
        Sdlevent.keysym = ke ;
        Sdlevent.keymod = kmod ;
        Sdlevent.keycode = kcode } 
       :: tail ->
            if List.mem ke symlist then
              (
               ui#delay_event 
                 (
                  Sdlevent.KEYDOWN
                  {
                   Sdlevent.ke_which = k_which;
                   Sdlevent.ke_state = state;
                   Sdlevent.keysym = ke ;
                   Sdlevent.keymod = kmod ;
                   Sdlevent.keycode = kcode ;
                   Sdlevent.unicode = int_of_char kcode}
                 );
                 next_in_action_list ui tail symlist result
                )
            else
               next_in_action_list ui tail (ke :: symlist)
                  ((ke, true) :: result) 
    | Sdlevent.KEYUP
      { 
        Sdlevent.ke_which = k_which;
        Sdlevent.ke_state = state; 
        Sdlevent.keysym = ke ;
        Sdlevent.keymod = kmod ;
        Sdlevent.keycode = kcode}
       :: tail ->
            if List.mem ke symlist then
              (
               ui#delay_event 
                 (
                  Sdlevent.KEYUP
                  {
                   Sdlevent.ke_which = k_which;
                   Sdlevent.ke_state = state;
                   Sdlevent.keysym = ke ;
                   Sdlevent.keymod = kmod ;
                   Sdlevent.keycode = kcode ;
                   Sdlevent.unicode = int_of_char kcode}
                 );  
                 next_in_action_list ui tail symlist result 
                )
            else
               next_in_action_list ui tail (ke :: symlist)
                  ((ke, false) :: result) 
    | head :: tail ->
         next_in_action_list ui tail symlist result 
;;

let get_acts_and_unprocessed ui =
   Sdlevent.pump ();
   let events_happened = (Sdlevent.get 32) @ ui#get_pending in
      ui#clear_pending;
      if events_happened = [] then [] 
      else
         let actions_done =
            collect_action_list ui events_happened
         in
            actions_done
;;

let width_height {Sdlvideo.flags = _;
                  Sdlvideo.w = w;
                  Sdlvideo.h = h;
                  Sdlvideo.pitch = _;
                  Sdlvideo.clip_rect = _;
                  Sdlvideo.refcount = _} =
   w, h
;;

let rec list_backspaced list =
   match list with
      [] -> []
    | element :: [] -> []
    | element :: l -> element :: list_backspaced l
;;

let rec pack_string count strlist newstring =
   match count with
      0 -> newstring
    | _ -> newstring.[count-1] <- List.nth strlist (count-1);
         pack_string (count-1) strlist newstring
;;

let string_of_list str =
   let len = List.length str in
      let newstring = String.create len in
         pack_string len str newstring
;;

let draw_character c x y rgb font charbox screen =
   let use_colour = 
      Sdlvideo.map_RGB screen Sdlvideo.black 
   in
   Sdlvideo.fill_rect charbox use_colour;
   let render = Sdlttf.SOLID rgb in
   let text = Sdlttf.render_text font render (String.make 1 c) in
   let (w, h, _) = Sdlvideo.surface_dims text in
   let r = Sdlvideo.rect ~x: 0 ~y: 0 ~w ~h in
   Sdlvideo.blit_surface ~src: text ~dst: charbox ~dst_rect: r ();
   let r2 = Sdlvideo.rect ~x: x ~y: y ~w ~h in
   Sdlvideo.blit_surface ~src: charbox ~dst: screen ~dst_rect: r2 ();
;;

let ffree_say strn x y rgb font screen =
   let render = Sdlttf.SOLID rgb in
   let text = Sdlttf.render_text font render strn in
   let (w, h, _) = Sdlvideo.surface_dims text in
   let r2 = Sdlvideo.rect ~x: x ~y: y ~w ~h in
   Sdlvideo.blit_surface ~src:text ~dst:screen ~dst_rect:r2 ();
   (x, y, w, h)
;;

let rec transfer_pixels src dst pl =
   match pl with
      [] -> []
    | (lx, ly) :: tail ->
        (
         let src_rect = Sdlvideo.rect ~x:lx ~y:ly ~w:1 ~h:1 in
         let dst_rect = Sdlvideo.rect ~x:lx ~y:ly ~w:1 ~h:1 in
         Sdlvideo.blit_surface ~src:src ~src_rect:src_rect 
               ~dst:dst ~dst_rect:dst_rect ();
         let drect =
            (Sdlvideo.rect
               ~x:lx ~y:ly ~w:1 ~h:1
            )
         in
         drect :: transfer_pixels src dst tail
        )
;;

let rec paint_pixels surf sdlcolour pl =
   match pl with
      [] -> []
    | (lx, ly) :: tail ->
        (
         Sdlvideo.put_pixel surf ~x:lx ~y:ly sdlcolour;
         let drect =
            (Sdlvideo.rect
               ~x:lx ~y:ly ~w:1 ~h:1
            )
         in
         drect :: paint_pixels surf sdlcolour tail
        )
;;

let sws_expand src dst expand_x expand_y =
   let (w, h, _) = Sdlvideo.surface_dims src in
(*
   Sdlvideo.lock src;
   Sdlvideo.lock dst;
*)
   for county = 0 to (h - 1) do
      for countx = 0 to (w - 1) do
         let px = Sdlvideo.get_pixel src countx county in
         for subcounty = 0 to (expand_y - 1) do
            for subcountx = 0 to (expand_x - 1) do
               Sdlvideo.put_pixel dst 
                     ~x:((countx * expand_x) + subcountx) 
                     ~y:((county * expand_y) + subcounty)
                     px
            done
         done
      done
   done;
(*
   Sdlvideo.unlock src;
   Sdlvideo.unlock dst;
*)
;;

class terminal (opts:Ui_opt.option) =
 let scr = sdl_init opts in
   object (self)
      val mutable surfaces = [("true", (ref scr) )]
      val nnnn = Sdlttf.init ()
      val font = Sdlttf.open_font (opts#get_key "fontfile")
            (int_of_string (opts#get_key "font_points"))
      val charbox = Sdlvideo.create_RGB_surface_format scr [] 
                    ~w:8 ~h:16
      val opt = opts
      val expand_x = opts#get_int "expand_x"
      val expand_y = opts#get_int "expand_y"
      val colour = S_colour.get_colourset opts
      val keyset = Key_sdl.new_keyset
      val menu_keyset = Key_sdl.menu_action_keyset ();
      val action_keyset = [Key_sdl.second_action_keyset ();
         Key_sdl.new_action_keyset ();
         Key_sdl.third_action_keyset ()]
      val mutable dirty = ([] : (Sdlvideo.rect list))
      val mutable graphics = ([]: (string * Sdlvideo.surface) list)
      val mutable pending_events = ([]: Sdlevent.event list)
      val virtual_width = opts#get_int "screen_width"
      val virtual_height = opts#get_int "screen_height"
      method cell_to_pixel xcell ycell =
         let xpixel = xcell * (opt#get_int "font_width") in
            let ypixel = (ycell * (opt#get_int "font_height")) in
               (xpixel, ypixel)
      method move_to_curs (cursor:Cursor.cursor) = ()
      method clear_cursor (cursor:Cursor.cursor) = ()
      method free_say_retbox strn x y target =
         let rgb = colour#get_colour (opt#get_colour "c_colour") in
         let srf = !(List.assoc target surfaces) in
         let nx, ny, nw, nh =
            ffree_say strn x y rgb font srf
         in 
         dirty <- 
            (Sdlvideo.rect ~x: nx ~y: ny ~w: nw ~h: nh) :: dirty;
         nx, ny, nw, nh
      method free_say strn x y target =
         ignore (self#free_say_retbox strn x y target)
      method print_ccodechr (character:int) (cursor:Cursor.cursor)
            target
      =
         match (Char.chr character) with
            '\n' -> cursor#newline ()
          | _ ->
            let x = cursor#getx + cursor#get_x_offset in
            let y = cursor#gety + cursor#get_y_offset in
            let dx, dy = self#cell_to_pixel x y in
               let uc = colour#get_colour (opt#get_colour "c_colour") in
               draw_character (Char.chr character) dx dy uc
                     font charbox !(List.assoc target surfaces);
               cursor#incx
      method print_ccodechr_up (character:int) (cursor:Cursor.cursor)
            target
      =
         match (Char.chr character) with
            '\n' -> cursor#lineup ()
          | _ ->
            let x = cursor#getx + cursor#get_x_offset in
            let y = cursor#gety + cursor#get_y_offset in
            let dx, dy = self#cell_to_pixel x y in
            let uc = colour#get_colour (opt#get_colour "c_colour") in
               draw_character (Char.chr character) dx dy uc
                     font charbox !(List.assoc target surfaces);
               cursor#incx
      method print_codechr (character:int) (cursor:Cursor.cursor)= ()
      method print_str (string:string) cursor target =
         for count = 0 to ((String.length string) -1) do
            self#print_ccodechr (Char.code string.[count]) cursor target
         done;
      method print_str_up (string:string) cursor target =
         for count = 0 to ((String.length string) -1) do
            self#print_ccodechr_up (Char.code string.[count]) cursor 
                  target
         done;
      method get_string cursor target =
         let str = ref [] in
            let character = ref ' ' in
               (while not (!character = Char.chr 13) do
                        (*FIXME - arena will not display *)
                  Sdlvideo.update_rect 
                        !(List.assoc target surfaces); 
                  let inkey = keyset#get_key (read_key opt) in
                  character :=
                     if inkey = "backsp" then '\008'
                     else if inkey = "enter" then Char.chr 13
                     else if String.length inkey = 0 then '?'
                     else
                        inkey.[0];
                  ;
                  if not (!character = Char.chr 13) then
                  (
                     match !character with
(*                        (Char.chr 13) -> () *)
                        '\008' ->
                        if List.length !str > 0 then
                           (
                            str := list_backspaced !str;
                            cursor#decx;
                            self#clear_cursorcell cursor target;
                            ()
                           )
                      | '?' -> ()
                      | _ ->
                        let c = Char.code !character in
                           if c < 256 then
                              (str := !str @ [!character];
                              self#print_ccodechr c cursor target;
                              ()
                              )
                  )
               done;
               string_of_list !str)
      method set_colour (colouri:int) =
         opt#set_colour "c_colour" colouri;
      method internal_set_colour (colouri:int) = ()
      method internal_unset_colour (colouri:int) = () 
      method imp_setup_screen = ()
      method imp_ask q cursor target = 
         (self#print_str q cursor target;
         self#print_str "> " cursor target;
         let a = self#get_string cursor in
            cursor#newline () ;
            a)
      method clear_cursorcell (cursor:Cursor.cursor) target =
         let x = cursor#getx + cursor#get_x_offset in
         let y = cursor#gety + cursor#get_y_offset in
            let dx, dy = self#cell_to_pixel x y in
            let uc = colour#get_colour (opt#get_colour "c_colour") in
               draw_character ' ' dx dy uc font charbox 
                     !(List.assoc target surfaces)
      method clear_cursorwindow (cursor:Cursor.cursor) target =
         let prevcolour = opt#get_colour "c_colour" in
            let x,y = self#cell_to_pixel cursor#get_x_offset
                                         cursor#get_y_offset in
               (
                self#set_colour (opt#get_colour "b_colour");
                let y_offset = ((opt#get_int "font_height") *
                 cursor#getboundy)
                 in
                  let use_colour = 
                   Sdlvideo.map_RGB 
                         !(List.assoc target surfaces) 
                         Sdlvideo.black 
                  in
                   Sdlvideo.fill_rect 
                      ~rect:
                      (make_rect (x, y,
                       ((opt#get_int "font_width") *
                        cursor#getboundx),
                       (y_offset + y)
                      )) 
                      !(List.assoc target surfaces) use_colour;
                self#set_colour prevcolour;
                ()
               )
      method load_image s = Sdlloader.load_image s
      method blit = Sdlvideo.blit_surface
      method in_key = keyset#get_key (read_key opt)
      method display_arena =
         Sdlvideo.blit_surface 
               ~src:!(List.assoc "arena" surfaces) 
               ~dst:!(List.assoc "direct" surfaces) ()
      method sync_screen = 
         if not ((expand_x > 0) || (expand_y > 0)) then
           (
            Sdlvideo.flip !(List.assoc "true" surfaces)
           )
         else
           (
            sws_expand !(List.assoc "direct" surfaces)
                  !(List.assoc "true" surfaces) expand_x expand_y;
            Sdlvideo.flip !(List.assoc "true" surfaces)
           )
      method display_image i x y target = 
         let width, height, _ = Sdlvideo.surface_dims i in
         let drect =
            (Sdlvideo.rect
               ~x:x ~y:y ~w:width ~h:height
            )
         in
         let srect = Sdlvideo.get_clip_rect i in
            dirty <- drect :: dirty;
            Sdlvideo.blit_surface ~src: i ~src_rect: srect 
                  ~dst: !(List.assoc target surfaces)
                  ~dst_rect: drect ()
      method display_partial_image i x y (subx, suby, w, h) target 
= 
(*         let width, height, _ = Sdlvideo.surface_dims i in *)
         let drect =
            (Sdlvideo.rect
               ~x:x ~y:y ~w:w ~h:h
            )
         in
         let srect = Sdlvideo.rect ~x:subx ~y:suby ~w:w ~h:h in
            dirty <- drect :: dirty;
            Sdlvideo.blit_surface ~src: i ~src_rect: srect 
                  ~dst: target
                  ~dst_rect: drect ()
      method apiblit i target x y (subx, suby, w, h) =
         let width, height, _ = Sdlvideo.surface_dims i in
         let aw, ah = if (w = 0) then (width, height) else (w, h) in
         let drect =
            (Sdlvideo.rect
               ~x:x ~y:y ~w:width ~h:height
            )
         in
         let tsurf = List.assoc target graphics in
         let srect = Sdlvideo.rect ~x:subx ~y:suby ~w:aw ~h:ah in
            dirty <- drect :: dirty;
            Sdlvideo.blit_surface ~src: i ~src_rect: srect ~dst: tsurf
                               ~dst_rect: drect ()
      method clean_dirty = 
         Sdlvideo.update_rects dirty !(List.assoc "arena" surfaces);
         dirty <- []
      method get_menu_action =
           let key_events =
              get_acts_and_unprocessed self
           in
              get_first_action key_events menu_keyset
      method get_actions (* pnum *) =
           let key_events =
              get_acts_and_unprocessed self
           in
              match_keys_with_keysets key_events action_keyset
      method clear_event_queue =
         Sdlevent.pump ();
         let events_ignored = Sdlevent.get 32 in
            Sdlevent.add pending_events;
            pending_events <- [];
            ignore events_ignored
      method begin_arena_mode =
         Sdlevent.enable_events Sdlevent.keydown_mask;
         Sdlevent.enable_events Sdlevent.keyup_mask;
(*         Sdlkey.enable_key_repeat ~delay:0 ~interval:0 (); *)
      method end_arena_mode = ()
(*
         Sdlkey.enable_key_repeat ()
*)
      method fix_colours sdlgraphic =
(* *)
         Sdlvideo.lock sdlgraphic;
            let transblack = 
               Sdlvideo.map_RGB sdlgraphic ~alpha:0 (0, 0, 0) 
            in
            let width, height, _ = Sdlvideo.surface_dims sdlgraphic in
            for countx = 0 to  (width - 1) do
               for county = 0 to (height - 1) do
                  let pix = 
                     Sdlvideo.get_pixel sdlgraphic countx county 
                  in
                  let (r, g, b), alpha = 
                     Sdlvideo.get_RGBA sdlgraphic pix 
                  in
(*                     if (alpha = 0) || *)
                     if ((r = 255) && (b = 255) && (g = 0)) then
                        Sdlvideo.put_pixel sdlgraphic 
                           ~x:countx ~y:county transblack
(*
                     else
                        Sdlvideo.put_pixel sdlgraphic 
                           ~x:countx ~y:county pix
*)
               done
            done;
            Sdlvideo.unlock sdlgraphic;
(* *)
            sdlgraphic
      method colour_gradient sdlgraphic grad (dr, dg, db) (lr, lg, lb) =
         let steps = (Array.length grad) - 1 in
         let rdiff = lr - dr in
         let gdiff = lg - dg in
         let bdiff = lb - db in
         let rstep = (float_of_int rdiff) /. (float_of_int steps) in
         let gstep = (float_of_int gdiff) /. (float_of_int steps) in
         let bstep = (float_of_int bdiff) /. (float_of_int steps) in
         for count = 0 to steps do
            grad.(count) <- Sdlvideo.map_RGB sdlgraphic 
                 (
                  (dr + int_of_float (rstep *. (float_of_int count))),
                  (dg + int_of_float (gstep *. (float_of_int count))),
                  (db + int_of_float (bstep *. (float_of_int count)))
                 )
         done
      method fix_material_colours gn =
         let sdlgraphic = List.assoc gn graphics in
         Sdlvideo.lock sdlgraphic;
            let black = 
               Sdlvideo.map_RGB sdlgraphic (0, 0, 0) 
            in
            let palebrown = Array.create 8 black in
            self#colour_gradient 
               sdlgraphic palebrown (128, 68, 8) (160, 88, 24);
            let darkbrown = Array.create 8 black in
            self#colour_gradient 
               sdlgraphic darkbrown (84, 40, 0) (96, 48, 0);
            let grey = Array.create 8 black in
            self#colour_gradient 
               sdlgraphic grey (84, 84, 84) (156, 156, 156);
            let width, height, _ = Sdlvideo.surface_dims sdlgraphic in
            for countx = 0 to  (width - 1) do
               for county = 0 to (height - 1) do
                  let pix = 
                     Sdlvideo.get_pixel sdlgraphic countx county 
                  in
                  let (r, g, b), alpha = 
                     Sdlvideo.get_RGBA sdlgraphic pix 
                  in
                  let rnd = Random.int 8 in
                  let worm_collide = not (r = 0) in
                  let bullet_collide = not (g = 0) in
                  let destroyable = not (b = 0) in
                  if (worm_collide || bullet_collide) 
                        && destroyable 
                  then
                     Sdlvideo.put_pixel sdlgraphic 
                           ~x:countx ~y:county darkbrown.(rnd)
                  else if (worm_collide || bullet_collide)
                        && (not destroyable)
                  then
                     Sdlvideo.put_pixel sdlgraphic 
                           ~x:countx ~y:county grey.(rnd)
                  else
                     Sdlvideo.put_pixel sdlgraphic 
                           ~x:countx ~y:county palebrown.(rnd)
               done
            done;
            Sdlvideo.unlock sdlgraphic;
(*
            sdlgraphic
*)
      method paint_alternate_colours
            (t:string) offx offy pl colour1 colour2 target
      =
(*FIXME - t:string and sdlgraphic not needed
         let sdlgraphic = List.assoc t graphics in
*)
         Sdlvideo.lock !(List.assoc target surfaces);
            let ca = Sdlvideo.map_RGB 
                  !(List.assoc target surfaces) colour1 in
            let cb = Sdlvideo.map_RGB 
                  !(List.assoc target surfaces) colour2 in
            let lista = Algo.splitlist pl false in
            let listb = Algo.splitlist pl true in
            let offset_lista = Algo.add_offset offx offy lista in
            let offset_listb = Algo.add_offset offx offy listb in
               ignore (paint_pixels 
                     !(List.assoc target surfaces) ca 
                     offset_lista);
               ignore (paint_pixels 
                     !(List.assoc target surfaces) cb 
                     offset_listb);
         Sdlvideo.unlock !(List.assoc target surfaces)
      method load_graphic nm f =
         let new_graphic =
            ( Sdlloader.load_image f )
         in
            let fixed_graphic = self#fix_colours new_graphic in
            graphics <- (nm, fixed_graphic) :: graphics
      method register_graphic nm g =
         graphics <- (nm, g) :: graphics
      method display_graphic (gn:string) (x:int) (y:int) target =
         if List.mem_assoc gn graphics then
            let graphic = List.assoc gn graphics in
               self#display_image graphic x y target
         else 
            print_string ("Graphical image " ^ gn ^ " not found.\n")
      method display_partial_graphic (gn:string) (x:int) (y:int) t 
            target 
      =
         if List.mem_assoc gn graphics then
            let graphic = List.assoc gn graphics in
               self#display_partial_image graphic x y t
                     !(List.assoc target surfaces)

         else 
            print_string ("partial Graphical image " 
                  ^ gn ^ " not found.\n")
      method blitover_graphic gn1 gn2 subbox x y =
         if List.mem_assoc gn1 graphics
         && List.mem_assoc gn2 graphics then
            let g1 = List.assoc gn1 graphics in
            let g2 = List.assoc gn2 graphics in
               self#display_partial_image g1 x y subbox g2
      method end_ui = ()
      method get_gdim gname =
        try
         if List.mem_assoc gname graphics then
            let g = List.assoc gname graphics in
               let si = Sdlvideo.surface_info g in
                  width_height si
         else 0,0
        with _ -> raise (Failure "Failed in ui_sdl.get_gdim")
      method get_pixel_rgb x y gname =
        try
         if List.mem_assoc gname graphics then
           (
            let g = List.assoc gname graphics in
               Sdlvideo.lock g;
               let (re:int), (gr:int), (bl:int) =
                  Sdlvideo.get_pixel_color g x y
               in 
                  Sdlvideo.unlock g;
                  re, gr, bl
           )
         else 0,0,0
        with _ -> raise (Failure "failed in ui_sdl.get_pixel_rgb")
      method get_pixel_rgba x y gname =
        try
         if List.mem_assoc gname graphics then
           (
            let g = List.assoc gname graphics in
            Sdlvideo.lock g;
            let pix = Sdlvideo.get_pixel g x y in
               let ((re:int), (gr:int), (bl:int)), alpha =
                  Sdlvideo.get_RGBA g pix 
               in
               Sdlvideo.unlock g;
               re, gr, bl, alpha
           )
         else 0,0,0,255
        with _ -> raise (Failure "failed in ui_sdl.get_pixel_rgb")
      method cover_box with_g (lx, ly, wdt, hgt) target =
         let wg = List.assoc with_g graphics in
(*
         let wdt,hgt = self#get_gdim cover_g in
*)
         let drect =
            (Sdlvideo.rect
               ~x:lx ~y:ly ~w:wdt ~h:hgt
            )
         in
         let srect = Sdlvideo.rect lx ly wdt hgt in
            dirty <- drect :: dirty;
            Sdlvideo.blit_surface ~src: wg ~src_rect: srect 
                  ~dst: !(List.assoc target surfaces)
                  ~dst_rect: drect ()
      method cover_sprite cover_g with_g lx ly target =
         let wg = List.assoc with_g graphics in
         let wdt,hgt = self#get_gdim cover_g in
         let drect =
            (Sdlvideo.rect
               ~x:lx ~y:ly ~w:wdt ~h:hgt
            )
         in
         let srect = Sdlvideo.rect lx ly wdt hgt in
            dirty <- drect :: dirty;
            Sdlvideo.blit_surface ~src: wg ~src_rect: srect 
                  ~dst: !(List.assoc target surfaces)
                  ~dst_rect: drect ()
      method delay = Sdltimer.delay
      method get_ticks = Sdltimer.get_ticks
      method paint_surface_and_bg_pixels static under target pl =
         if (List.mem_assoc static graphics)
         && (List.mem_assoc under graphics) then
           (
            let dest = List.assoc static graphics in
            let src = List.assoc under graphics in
            ignore(transfer_pixels src dest pl);
            dirty <- dirty @ (transfer_pixels src 
                  !(List.assoc target surfaces) pl);
           )
      method paint_surface_pixel surf lx ly colour =
         let sdlcolor = Sdlvideo.map_RGB surf colour in
         Sdlvideo.lock surf;
         Sdlvideo.put_pixel surf ~x:lx ~y:ly sdlcolor;
         Sdlvideo.unlock surf
      method paint_pixel lx ly colour target =
         let sdlcolor = 
               Sdlvideo.map_RGB !(List.assoc target surfaces) colour 
         in
         Sdlvideo.lock !(List.assoc target surfaces);
         Sdlvideo.put_pixel !(List.assoc target surfaces) 
               ~x:lx ~y:ly sdlcolor;
         Sdlvideo.unlock !(List.assoc target surfaces)
      method get_pending =
            pending_events
      method clear_pending =
            pending_events <- []
      method delay_event ev = 
         pending_events <- ev :: pending_events
(*
         Sdlevent.add [ev]
*)
      method conditional_borderline_graphic gname 
            (colour:Sdlvideo.color) 
      =
        try
         if List.mem_assoc gname graphics then
            let g = List.assoc gname graphics in
               let width, height = self#get_gdim gname in
               let sdlcolor = Sdlvideo.map_RGB g colour in
               Sdlvideo.lock g;
               for count = 0 to (width - 1) do
(* FIXME
                  let (re:int), (gr:int), (bl:int) =
                     Sdlvideo.get_pixel_color g count (height - 1)
                  in
                  if (re = 0) && (gr = 0) && (bl = 0) then
*)
                     Sdlvideo.put_pixel
                        g ~x:count ~y:(height - 1) sdlcolor;
                     Sdlvideo.put_pixel
                        g ~x:count ~y:0 sdlcolor;
                     
               done;
               for count = 0 to (height - 1) do
(*
                  let (re:int), (gr:int), (bl:int) =
                     Sdlvideo.get_pixel_color g count (height - 1)
                  in
*)
                     Sdlvideo.put_pixel
                        g ~x:(width - 1) ~y:count sdlcolor;
                     Sdlvideo.put_pixel
                        g ~x:0 ~y:count sdlcolor;
               done;
               Sdlvideo.unlock g
        with _ -> raise (Failure 
           "failed in ui_sdl.conditional_underline_graphic")
      method create_composite newname under over =
        try
         let under_g = List.assoc under graphics in
         let over_g = List.assoc over graphics in
         let w, h, _ = Sdlvideo.surface_dims under_g in
         let new_g = 
            Sdlvideo.create_RGB_surface_format under_g [] ~w:w ~h:h
         in
            Sdlvideo.blit_surface ~src:under_g ~dst:new_g ();
            Sdlvideo.blit_surface ~src:over_g ~dst:new_g ();
            graphics <- (newname, new_g) :: graphics
        with _ -> raise (Failure
           "failed in ui_sdl.create_composite")
      method new_arena x y =
         (List.assoc "arena" surfaces) :=
            Sdlvideo.create_RGB_surface_format 
                  scr [`SWSURFACE] x y
(*
         (List.assoc "direct" surfaces) :=
            if expand then
               ref (Sdlvideo.create_RGB_surface_format
                  scr [`SWSURFACE] x y)
            else ref scr
*)
      method arena_to_screen (x, y, w, h) (dest_x, dest_y) =
         let drect =
            (Sdlvideo.rect
               ~x:dest_x ~y:dest_y ~w:w ~h:h
            )
         in
         let srect = Sdlvideo.rect ~x:x ~y:y ~w:w ~h:h in
            Sdlvideo.blit_surface 
                  ~src: !(List.assoc "arena" surfaces) 
                  ~src_rect: srect 
                  ~dst: !(List.assoc "direct" surfaces) 
                  ~dst_rect: drect ()
      method set_alpha s n =
         try
            let g = List.assoc s graphics in
               Sdlvideo.set_alpha g n
         with Not_found -> 
            print_string "\nError! set_alpha failed on Not_found"
      method fill_rect coords srf (r, g, b, a) =
         try
            let s = !(List.assoc srf surfaces) in
         let rectangle = make_rect coords in
         let c = 
          match a with | 255 -> Sdlvideo.map_RGB s (r, g, b) 
           | _ -> Sdlvideo.map_RGB s ~alpha:a (r, g, b) 
         in
            Sdlvideo.fill_rect ~rect:rectangle s c
         with Not_found -> 
            print_string "\nError! Ui_sdl.fill_rect failed on Not_found"
      initializer
         let (x, y, z) = Sdlvideo.surface_dims scr in
         let arena = 
            Sdlvideo.create_RGB_surface_format 
                  scr [`SWSURFACE] x y
         in
         let buffer =
            if opts#get_bool "expand" then
               ref (Sdlvideo.create_RGB_surface_format
                  scr [`SWSURFACE] virtual_width virtual_height)
            else ref scr
         in
            surfaces <- ("arena", (ref arena) ) 
                  :: ("direct", buffer) :: surfaces

   end
;;

let new_terminal opts =
   new terminal opts
;;
