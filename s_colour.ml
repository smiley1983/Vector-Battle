(*
    Jude's User Interface
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Provides colour translation for the SDL interface

*)


class colourset option =
   object
      val option = option
      val colourset =
         [(1, Sdlvideo.black);
          (2,Sdlvideo.red);
          (3,Sdlvideo.green);
          (5,Sdlvideo.blue);
          (8, (192, 192, 192));
          (4 , (192, 96, 48));
          (7 , Sdlvideo.cyan);
          (6, Sdlvideo.magenta);
          (9, (80, 80, 80));
          (10, Sdlvideo.yellow);
          ]
      method get_colour cstr =
         try
            List.assoc cstr colourset
         with Not_found ->
            (80, 80, 80)
   end
;;

let get_colourset = new colourset
