(*
    Jude's User Interface
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Provides key conversions for the SDL interface.

*)


class keyset =
   object
      method get_key (character:int) =
         match character with
            8 -> "backsp"
          | 13 -> "enter"
          | 264 -> "keypad_north"
          | 258 -> "keypad_south"
          | 260 -> "keypad_west"
          | 262 -> "keypad_east"
          | 257 -> "keypad_sw"
          | 259 -> "keypad_se"
          | 263 -> "keypad_nw"
          | 265 -> "keypad_ne"
          | _ -> 
               if (character > 31) && (character < 256) then
                  String.make 1 (Char.chr character)
               else ""
   end
;;


let new_keyset = new keyset;;

class action_keyset =
   object
      val rev_ak = Hashtbl.create 9
      method default_init =
         Hashtbl.add rev_ak Sdlkey.KEY_DOWN Olevent.Down ;
         Hashtbl.add rev_ak Sdlkey.KEY_UP Olevent.Up;
         Hashtbl.add rev_ak Sdlkey.KEY_LEFT Olevent.Left;
         Hashtbl.add rev_ak Sdlkey.KEY_RIGHT Olevent.Right;
         Hashtbl.add rev_ak Sdlkey.KEY_SLASH Olevent.Fire;
         Hashtbl.add rev_ak Sdlkey.KEY_COMMA Olevent.Change;
         Hashtbl.add rev_ak Sdlkey.KEY_PERIOD Olevent.Rope;
         Hashtbl.add rev_ak Sdlkey.KEY_SEMICOLON Olevent.Jump;
         Hashtbl.add rev_ak Sdlkey.KEY_ESCAPE Olevent.Quit
      method init_key sdlk oe = 
         Hashtbl.add rev_ak sdlk oe
      method rev_key a =
         try Hashtbl.find rev_ak a with Not_found -> Olevent.Nothing
      method contains a =
         Hashtbl.mem rev_ak a
   end
;;

let second_action_keyset () = 
   let aks = new action_keyset in 
   aks#default_init;
   aks
;;

let new_action_keyset () = 
   let aks = new action_keyset in
      aks#init_key Sdlkey.KEY_r Olevent.Up;
      aks#init_key Sdlkey.KEY_f Olevent.Down;
      aks#init_key Sdlkey.KEY_d Olevent.Left;
      aks#init_key Sdlkey.KEY_g Olevent.Right;
      aks#init_key Sdlkey.KEY_c Olevent.Fire;
      aks#init_key Sdlkey.KEY_x Olevent.Jump;
      aks#init_key Sdlkey.KEY_z Olevent.Change;
      aks#init_key Sdlkey.KEY_ESCAPE Olevent.Quit;
      aks
;;

let third_action_keyset () = 
   let aks = new action_keyset in
      aks#init_key Sdlkey.KEY_i Olevent.Up;
      aks#init_key Sdlkey.KEY_k Olevent.Down;
      aks#init_key Sdlkey.KEY_j Olevent.Left;
      aks#init_key Sdlkey.KEY_l Olevent.Right;
      aks#init_key Sdlkey.KEY_m Olevent.Fire;
      aks#init_key Sdlkey.KEY_n Olevent.Jump;
      aks#init_key Sdlkey.KEY_b Olevent.Change;
      aks#init_key Sdlkey.KEY_ESCAPE Olevent.Quit;
      aks
;;

let menu_action_keyset () = 
   let aks = new action_keyset in
      aks#init_key Sdlkey.KEY_UP Olevent.Up;
      aks#init_key Sdlkey.KEY_DOWN Olevent.Down;
      aks#init_key Sdlkey.KEY_LEFT Olevent.Left;
      aks#init_key Sdlkey.KEY_RIGHT Olevent.Right;
      aks#init_key Sdlkey.KEY_RETURN Olevent.Fire;
      aks#init_key Sdlkey.KEY_SLASH Olevent.Fire;
      aks#init_key Sdlkey.KEY_COMMA Olevent.Fire;
      aks#init_key Sdlkey.KEY_ESCAPE Olevent.Quit;
      aks
;;
