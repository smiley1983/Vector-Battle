(*
    Jude's User Interface
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Universal colour handling. It needs a serious overhaul.

*)


let conv_colour colour_int =
   match colour_int with
     2 -> "red"
    | 3 -> "green"
    | 4 -> "yellow"
    | 5 -> "blue"
    | 6 -> "magenta"
    | 7 -> "cyan"
    | 1 -> "black"
    | 9 -> "darkgrey"
    | 10 -> "brightyellow"
    | _ -> "white"
;;

let conv_colour_s colourstring =
   match colourstring with
     "red" -> 2
    | "green" -> 3
    | "yellow" -> 4
    | "blue" -> 5
    | "magenta" -> 6
    | "cyan" -> 7
    | "black" -> 1
    | "darkgrey" -> 9
    | "brightyellow" -> 10
    | _ -> 8 (*white*)
;;
