(*
    Vector Battle
    Copyright (C) 2006 - 2010  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

*)

(*

*  What does this file do?

**    Provides collision detection and resolution functions.

*)

(* 
The basic wall_collision function checks to see if any pixel overlap 
occurs, and returns the points of the overlap (if any) plus a boolean 
for whether or not they collided. 
*)

let wall_collide_column maskget xval collider_col player_col 
      st fin mask_y is_worm 
=
  try
  (
   let points = ref [] in
(*
   let max_y = ref 0 in
   let min_y = ref 0 in
*)
   let collides = ref false in
   for count = st to fin do
      let (worm_collide, bullet_collide, _) = 
         maskget collider_col count 
      in
      let mask_elem = Array.get player_col ((count - st) + mask_y) in
         if ((worm_collide && is_worm) 
               || (bullet_collide && (not is_worm))
            ) && mask_elem 
         then
           (
(*
            let cpos = (count - st) + 1 in
            if !max_y < cpos then max_y := cpos;
            if (!min_y == 0) || (!min_y > cpos) then 
               min_y := cpos;
*)
            collides := true;
            points := (xval, count) :: !points
           )
   done;
   !collides, !points
  )
  with n -> 
     raise (Failure ("in Collide.wall_collide_column: " ^ 
           Printexc.to_string n))
;;

let wall_collide_row maskget collider_mask player_mask st ly fin 
      player_rect is_worm 
=
  try
  (
   let points = ref [] in
(*
   let max_x = ref 0 in let max_y = ref 0 in 
   let min_x = ref 0 in let min_y = ref 0 in
*)
   let does_collide = ref false in
   let sample_row = Array.get collider_mask 0 in
   let wall_height = Array.length sample_row in
(*
   let sample_player_mask = Array.get player_mask 0 in
*)
   let player_width = Array.length player_mask in
   let mx, my, mw, mh = player_rect in
   let mask_height = 
(*
         if mh = 0 then Array.length sample_player_mask else mh 
*)
      mh
   in
   let finy = 
      max 0 (
         if (ly + mask_height) >= wall_height then (wall_height - 1)
         else (ly + mask_height) - 1
      )
   in
   let finx = 
      if (fin - st) + mx < player_width then fin 
      else ((player_width + st) - mx) - 1
   in
   for count = st to finx do
      let w_column = 
        try (
         Array.get collider_mask count 
        ) with e -> raise (Failure ("Eeee!collider_mask" 
               ^ (string_of_int count)))
      in
      let m_column = 
        try (
         Array.get player_mask ((count - st) + mx) 
        ) with e -> raise (Failure ("Eeee!player_mask" 
               ^ (string_of_int ((count - st) + mx)) ^ "  "
               ^ (string_of_int fin) ^ " "
               ^ (string_of_int (mx - st))
               ))
      in
         let collides, (*this_max_y, this_min_y, *) new_points =
            wall_collide_column maskget count w_column m_column ly finy 
                  my is_worm
         in
            if collides then
              (
               does_collide := true;
               points := !points @ new_points;
(*
               if !min_x = 0 then min_x := ((count - st) + 1);
               if !max_x < ((count - st) + 1) then 
                  max_x := ((count - st) + 1);
               if this_max_y > !max_y then max_y := this_max_y;
               if (not (this_min_y == 0)) then
                  if (!min_y == 0) || (!min_y > this_min_y) then
                     min_y := this_min_y
*)
              )
   done;
   (*!max_x, !max_y, !min_x, !min_y,*)
   (st, ly), !points, !does_collide
  )
  with n -> 
     raise (Failure ("in Collide.wall_collide_row: " ^ 
           Printexc.to_string n))
;;

(*
When called by bullet_player_collide, collider_mask is the bullet and 
player_mask is the player.
When called from update_projectile, collider_mask is the map, and 
player_mask is the bullet.
When called from the Player module, collider_mask is the map and 
player_mask is the player.
*)

let wall_collision maskget collider_mask player_mask 
      collider_lx collider_ly player_rect is_worm 
=
  try
  (
   let mx, my, mw, mh = player_rect in
   let collider_width = Array.length collider_mask in
(*   let mask_w_imit = Array.length player_mask - 1 in *) 
   let player_width = 
(*      if mw = 0 then Array.length player_mask else mw in
*)
      mw
   in
      let finx = 
           (
            if collider_lx + player_width >= collider_width then 
               (collider_width - 1)
            else (collider_lx + player_width) - 1
           )
      in
         wall_collide_row maskget collider_mask player_mask 
               (max 0 collider_lx) 
               (max 0 collider_ly) finx 
               player_rect is_worm
  )
  with n -> 
     raise (Failure ("in Collide.wall_collision: " ^ 
           Printexc.to_string n))
;;

let wall_maskget n count = Array.get n count;;

let worm_maskget n count = match Array.get n count with
   true -> (true, true, true)
 | false -> (false, false, false)
;;

let rec explode_bool_points bool_map pl =
   match pl with
      [] -> []
    | (lx, ly) :: tail ->
         let row = Array.get bool_map lx in
         let _, _, destroyable = Array.get row ly in
            if destroyable then
            (
               Array.set row ly (false, false, false);
               (lx, ly) :: explode_bool_points bool_map tail
            )
            else
               explode_bool_points bool_map tail
;;

let test_corner (x, y) (x1, y1, x2, y2) =
   (x >= x1) && (x <= x2) && (y >= y1) && (y <= y2)
;;

let rect_overlap (ax1, ay1, aw, ah) (bx1, by1, bw, bh) =
(*
   let x2b = x2 + w2 in let y2b = y2 + h2 in
      test_corner (x1, y1) (x2, y2, x2b, y2b)
   || test_corner ((x1 + w1), y1) (x2, y2, x2b, y2b)
   || test_corner (x1, (y1 + h1)) (x2, y2, x2b, y2b)
   || test_corner ((x1 + w1), (y1 + h1)) (x2, y2, x2b, y2b)
*)
   let ax2 = ax1 + aw in let ay2 = ay1 + ah in
   let bx2 = bx1 + bw in let by2 = by1 + bh in
      (ax1 < bx2) && (ax2 > bx1) && (ay1 < by2) && (ay2 > by1)
;;

let sub_rect (x1, y1, w1, h1) (x2, y2, w2, h2) =
   let x = max x1 x2 in
   let y = max y1 y2 in
   let w = (min (x1 + w1) (x2 + w2)) - x in 
   let h = (min (y1 + h1) (y2 + h2)) - y in
   x, y, w, h
;;

let prepare_vars_bpcol shot_bitmask bitmask bullet bullet_rect
      player player_rect
=
  try
  (
   let subx, suby, subw, subh = sub_rect bullet_rect player_rect in
   let bx, by, bw, bh = bullet_rect in
   let (bofx, bofy, _, _) = bullet#get_img_offset in
   let bofx2 = subx - bx in
   let bofy2 = suby - by in
   let bullet_targetx = bofx + bofx2 in
   let bullet_targety = bofy + bofy2 in
   let (offx, offy) = player#get_img_offset in
   let player_bitmask = 
      (List.assoc player#get_graphic bitmask) 
   in
(*
   let player_locx = player#int_locx in
   let player_locy = player#int_locy in
*)
   let player_locx, player_locy, _, _ = player_rect in
   let offx2 = subx - player_locx in
   let offy2 = suby - player_locy in
   let player_targetx = offx + offx2 in
   let player_targety = offy + offy2 in
      ((player_targetx, player_targety, subw, subh), 
       (bullet_targetx, bullet_targety), player_bitmask)
  )
  with n -> 
     raise (Failure ("in Collide.prepare_vars_bpcol: " ^ 
           Printexc.to_string n))
;;

let rec bullet_player_collide shot_bitmask bitmask bullet bullet_rect 
      players 
=
  try
  (
   match players with
      [] -> []
    | head :: tail ->
         let player_rect = head#get_img_rect in
         if rect_overlap bullet_rect player_rect then
           (
            let (player_targetx, player_targety, subw, subh),
                  (bullet_targetx, bullet_targety), player_bitmask 
            =
               prepare_vars_bpcol shot_bitmask bitmask bullet 
                     bullet_rect head player_rect
            in
(*            let bulletx, bullety, _, _ = bullet#get_rect in *)
            let _, cpoints, confirmed_hit =
(*
              if ( (not (subw = 0)) && (not (subh = 0)) ) then
*)
               wall_collision worm_maskget shot_bitmask player_bitmask
                     bullet_targetx bullet_targety 
                     (player_targetx, player_targety, subw, subh) 
                     false
(*
              else [], false
*)
            in
            if confirmed_hit then
              (
               let subrx, subry, _, _ = sub_rect bullet_rect player_rect 
               in
               let subpx, subpy, _, _ = player_rect in
               (((subrx - subpx), (subry - subpy)), cpoints, head )
               :: bullet_player_collide shot_bitmask bitmask bullet 
                     bullet_rect tail
              )
            else
              (
               bullet_player_collide shot_bitmask bitmask bullet 
                     bullet_rect tail
              )
           )
         else
            bullet_player_collide shot_bitmask bitmask bullet 
                  bullet_rect tail
  )
  with n -> 
     raise (Failure ("in Collide.bullet_player_collide: " ^ 
           Printexc.to_string n))
;;

let rec collide_bullet_and_players ui bullet worms =
   match worms with
      [] -> []
    | ((subx, suby), cpoints, head) :: tail ->
        (
         let blood = head#take_hit bullet in
         blood @ collide_bullet_and_players ui bullet tail
        )
;;

let bounce_bullet bullet wall_maskget bool_map sbm 
   (prev_locx, prev_locy) brect
=
   let velocx = bullet#get_veloc_x in
   let velocy = bullet#get_veloc_y in
   let _, _, strike_previous =
      wall_collision wall_maskget bool_map sbm 
            prev_locx prev_locy brect false
   in
   let _, _, strike_x =
      wall_collision wall_maskget bool_map sbm 
            prev_locx bullet#get_locy brect false
   in
   let _, _, strike_y =
      wall_collision wall_maskget bool_map sbm 
            bullet#get_locx prev_locy brect false
   in
      if not strike_x then 
         bullet#set_veloc_x (-.velocx *. bullet#get_bounce)
      else
         bullet#set_veloc_x (velocx *. bullet#get_bounce);
      if not strike_y then 
         bullet#set_veloc_y (-.velocy *. bullet#get_bounce)
      else
         bullet#set_veloc_y (velocy *. bullet#get_bounce);
      bullet#set_locx (float_of_int prev_locx);
      bullet#set_locy (float_of_int prev_locy);
      if not strike_previous then
         bullet#set_anim_speed (bullet#get_anim_speed *. 2.)
      else 
        (
(*
         print_string "\nblah\n";
*)
         bullet#dead_stop
        )
;;

let expiry_consequences head m_weaponlist =
  (
   let new_exps = 
      if head#explodes_on_timeout then
         head#explode [head#get_sprite_centre] 
      else []
   in
      Weapon.make_new_particles_mk2 m_weaponlist new_exps
  )
;;

let trail_particles head m_weaponlist =
  (
   let new_exps = head#trail_now in
      Weapon.make_new_particles_mk2 m_weaponlist new_exps
  )
;;

(*Note - expiry_p isn't added everywhere and perhaps it should be.*)

let rec update_projectile ui 
      (m_bulletlist : (string * Weapon.bullet_specs) list)
      (m_weaponlist : (string * Weapon.launcher) list)
      bitmask players bool_map xbound ybound gravity elapsed 
      projectile_list
=
  try
  (
   match projectile_list with
      [] -> []
    | head :: tail ->
         if head#is_alive then
           (
            let prev_locx = head#get_locx in
            let prev_locy = head#get_locy in
            head#move elapsed;
            let expiry_p =
               if head#is_expired then
                 (
                  head#kill; 
                  expiry_consequences head m_weaponlist
                 )
               else []
            in
            let trail_p = trail_particles head m_weaponlist in
            let particles = expiry_p @ trail_p in
            if (not (head#in_bounds xbound ybound))
            && head#dies_out_of_bounds then
              (
               head#kill;
               particles @ update_projectile ui m_bulletlist  
                     m_weaponlist bitmask players bool_map xbound ybound 
                     gravity elapsed tail
              )
            else if (List.mem_assoc head#get_graphic bitmask) then
              (
               let sbm =
                  (List.assoc head#get_graphic bitmask )
               in
               let _, wall_points, wall_collide =
                  wall_collision wall_maskget bool_map
                     sbm head#get_locx head#get_locy 
                     head#get_rect false
               in
               let worm_collide = 
                if head#damage_now then
                  bullet_player_collide sbm bitmask head 
                        head#get_img_rect players 
                else []
               in
                if (not wall_collide) 
                && (List.length worm_collide = 0) 
                then
                  (
                   if head#is_rope then head#set_sticky 0.;
(*                   head#drag elapsed; *)
                   head#gravity_pull gravity elapsed;
                   if (head#get_time_to_live > 0.0) 
                   && head#is_blood then
                      head#set_time_to_live 0.0;
                   particles @ update_projectile ui m_bulletlist
                         m_weaponlist bitmask players bool_map xbound 
                         ybound gravity elapsed tail
                  )
                else if (List.length worm_collide) > 0 && wall_collide 
                then
                  (
                   let blood = 
                      wormhit_consequences ui m_weaponlist head 
                            worm_collide 
                   in
                   particles @ blood @ 
                   wallhit_consequences ui m_bulletlist m_weaponlist
                         bitmask sbm players bool_map xbound ybound 
                         gravity elapsed tail head wall_points 
                         (prev_locx, prev_locy)
                  )
                else if (List.length worm_collide) > 0 then
                  (
                   let blood = 
                      wormhit_consequences ui m_weaponlist head 
                            worm_collide 
                   in
                      particles @ blood @ update_projectile ui 
                            m_bulletlist m_weaponlist bitmask players 
                            bool_map xbound ybound gravity elapsed tail
                  )
                else if wall_collide then
                  (
                   particles @ wallhit_consequences ui m_bulletlist
                         m_weaponlist bitmask sbm players bool_map 
                         xbound ybound gravity elapsed tail head 
                         wall_points (prev_locx, prev_locy)
                  )
                else
                      particles @ update_projectile ui m_bulletlist
                            m_weaponlist bitmask players bool_map xbound 
                            ybound gravity elapsed tail
              )
            else
              (
               print_string 
                  "could not find thing in Collide.update_projectile\n";
               update_projectile ui m_bulletlist m_weaponlist bitmask 
                     players bool_map xbound ybound gravity elapsed tail
              )
           )
         else
            update_projectile ui m_bulletlist m_weaponlist
                     bitmask players bool_map 
                     xbound ybound gravity elapsed tail
  )
  with n -> 
     print_string ("Failure in Collide.update_projectile: " ^ 
           (Printexc.to_string n));
     []
and wormhit_consequences ui m_weaponlist head worm_collide =
  (
   
   if head#dies_on_wormhit then head#kill
   else head#reset_damage_pulse;
   collide_bullet_and_players ui head worm_collide
   @ (if head#explodes_on_wormhit then 
      let ((_, _), cpoints, worm) = List.nth worm_collide 0 in
      let cplen = (List.length cpoints - 1) in
      let rpoint = if cplen > 1 then (Random.int cplen) else 0 in
      let (px, py) = List.nth cpoints rpoint in
      let ox, oy = head#get_int_loc in
         Weapon.make_new_particles_mk2 m_weaponlist
            (head#explode [((ox + px), (oy + py))])
   else []
   )
  )
and wallhit_consequences ui m_bulletlist m_weaponlist bitmask sbm 
      players bool_map xbound ybound gravity elapsed tail head 
      wall_points (prev_locx, prev_locy)
=
  (
    let was_decoration = head#is_blood in
    if not was_decoration then
      (
       if head#dies_on_wormhit then head#kill;
       if (head#get_bounce > 0.) && (not head#is_rope) then
          bounce_bullet head
             wall_maskget bool_map sbm (prev_locx, prev_locy)
             head#get_rect
       else head#dead_stop;
       if head#is_rope then head#set_sticky 1.;
       if head#explodes_on_ground then
       (
          let new_exps = head#explode wall_points in
          let new_p = Weapon.make_new_particles_mk2 
                m_weaponlist new_exps
          in
          let removed = 
             explode_bool_points bool_map wall_points 
          in
          ui#remove_pixels "static_bg" removed;
          new_p @ update_projectile ui m_bulletlist
                m_weaponlist bitmask players bool_map 
                xbound ybound gravity elapsed 
                (new_p @ tail)
       )
       else update_projectile ui m_bulletlist
                m_weaponlist bitmask players bool_map 
                xbound ybound gravity elapsed tail
      )
    else
      (
       let notblood = 
          let r, g, b = 
             ui#get_pixel_rgb head#get_locx head#get_locy 
                   "static_bg"
          in
          if (g = 0) && (b = 0) && (r > 0) then false
          else true
       in
       if notblood then
       (
          ui#blitover_graphic head#get_graphic 
                "static_bg" head#get_img_offset 
                head#get_locx head#get_locy;
          ui#cover_old_sprite "levelbox" 
                head#get_graphic "static_bg"
                head#get_locx head#get_locy;
       head#kill;
       update_projectile ui m_bulletlist 
             m_weaponlist bitmask players 
             bool_map xbound ybound gravity elapsed tail
       )
      else 
        (
         if head#get_time_to_live = 0.0 then
           (
            head#set_time_to_live 90.0;
            head#drag 1.0 0.5
           );
         head#gravity_pull (gravity /. 4.) elapsed;
         let vx = head#get_veloc_x in
         let vy = head#get_veloc_y in
         if (vx *. vx) +. (vy *. vy) > 0.005 then
           (
            head#set_veloc_x (vx *. 0.8);
            head#set_veloc_y (vy *. 0.8);
           );
         update_projectile ui m_bulletlist m_weaponlist 
            bitmask players bool_map xbound ybound 
            gravity elapsed tail
        )
      )
  )
;;

