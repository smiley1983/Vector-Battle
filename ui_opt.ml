(*
    Jude's User Interface
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    UI option handling

*)

let check_special s =
   match s with
      "hash" -> "#"
    | _ -> s
;;

let default_opt v =
   match v with
    | "terminaltype" -> "curses"
    | "f_colour" -> "grey"
    | "b_colour" -> "black"
    | "c_colour" -> "grey"
    | "font_width" -> "8"
    | "font_height" -> "16"
    | "screen_height" -> "480"
    | "screen_chars_high" -> "25"
    | "screen_chars_wide" -> "79"
    | "is_curses_compile" -> "false"
    | "map_x" -> "0"
    | "map_y" -> "0"
    | "map_wide" -> "25"
    | "map_high" -> "25"
    | "stat_x" -> "25"
    | "stat_y" -> "17"
    | "stat_wide" -> "54"
    | "stat_high" -> "8"
    | "msg_x" -> "25"
    | "msg_y" -> "0"
    | "msg_wide" -> "54"
    | "msg_high" -> "16"
    | "menu_fc" -> "green"
    | "menu_bc" -> "grey"
    | "menu_hc" -> "cyan"
    | "shift_bq" -> "~"
    | "shift_1" -> "!"
    | "shift_2" -> "@"
    | "shift_3" -> "hash"
    | "shift_4" -> "$"
    | "shift_5" -> "%"
    | "shift_6" -> "^"
    | "shift_7" -> "&"
    | "shift_8" -> "*"
    | "shift_9" -> "("
    | "shift_0" -> ")"
    | "shift_minus" -> "_"
    | "shift_equals" -> "+"
    | "shift_bs" -> "|"
    | "shift_[" -> "{"
    | "shift_]" -> "}"
    | "shift_;" -> ":"
    | "shift_'" -> "\""
    | "shift_," -> "<"
    | "shift_." -> ">"
    | "shift_/" -> "?"
    | "," -> "key_get"
    | "?" -> "key_help"
    | "kp8" -> "key_previous"
    | "kp2" -> "key_next"
    | "h" -> "key_previous"
    | "kp6" -> "key_next"
    | "kp7" -> "key_nw"
    | "kp9" -> "key_ne"
    | "kp1" -> "key_sw"
    | "kp3" -> "key_se"
    | "w" -> "key_longwalk"
    | "M" -> "key_viewmap"
    | "l" -> "key_look"
    | "ctrl+q" -> "key_quit"
    | ">" -> "key_downstair"
    | "<" -> "key_upstair"
    | "i" -> "key_inventory"
    | "d" -> "key_drop"
    | "D" -> "key_multidrop"
    | "a" -> "key_attack"
    | "-" -> "key_previous"
    | "=" -> "key_next"
    | "/" -> "key_switch"
    | "m" -> "key_skills"
    | "t" -> "key_throw"
    | "." -> "key_enter"
    | "Z" -> "key_cast_spell"
    | "e" -> "key_equipment"
    | "q" -> "key_drink"
    | "r" -> "key_read"
    | "v" -> "key_examine"
    | "p" -> "key_pass"
    | "alt+a" -> "wizard_show_actionq"
    | "alt+v" -> "key_wizard_vision"
    | "fast_menu" -> "true"
    | "is_wizard" -> "false"
    | "wizard_vision" -> "false"
    | "show_debug" -> "false"
    | "message_reverse" -> "true"
    | "stack_messages" -> "false"
    | "old_messages" -> "true"
    | "scenario" -> "Banishment"
    | "fontfile" -> "vtsr____.ttf"
    | "font_points" -> "16"
    | "fullscreen" -> "false"
    | "expand" -> "false"
    | "expand_x" -> "0"
    | "expand_y" -> "0"
    | "show_animation" -> "normal"
    | "graphics" -> "base"
    | "mapmode" -> "scroll"
    | "big_window" -> "msg"
    | _ -> "option_error"
;;


class option =
   object (self)
      val oset = Hashtbl.create 30
      val rev_oset = Hashtbl.create 30
      method add_option v1 v2 =
        (
         Hashtbl.replace oset v1 v2;
         Hashtbl.replace rev_oset v2 v1
        )
      method has_opt o =
         try
            ignore ( Hashtbl.find oset o );
            true
         with Not_found -> false
      method get_key v =
         try
            Hashtbl.find oset v
         with Not_found ->
            default_opt v
      method get_reverse_key v =
         try
            Hashtbl.find rev_oset v
         with Not_found ->
            "reverse option error"
      method get_bool v =
         try
            let a = Hashtbl.find oset v in
            if a = "true" then true else false
         with Not_found ->
            false
      method get_int v =
         try
            let a = Hashtbl.find oset v in
               int_of_string a
         with _ ->
            -1
      method get_colour v =
         try
            let a = Hashtbl.find oset v in
               U_colour.conv_colour_s a
         with _ ->
            U_colour.conv_colour_s "grey"
      method set_colour v colour = 
         Hashtbl.replace oset v (U_colour.conv_colour colour)
      method hard_init =
         self#add_option "terminaltype" "curses"(*"basic_graph"*);
         self#add_option "f_colour" "grey";
         self#add_option "b_colour" "black";
         self#add_option "c_colour" "grey";
         self#add_option "font_width" "8";
         self#add_option "font_height" "16";
         self#add_option "screen_height" "480";
         self#add_option "screen_width" "640";
         self#add_option "screen_chars_high" "25";
         self#add_option "screen_chars_wide" "79";
         self#add_option "is_curses_compile" "false";
         self#add_option "map_x" "0";
         self#add_option "map_y" "0";
         self#add_option "map_wide" "25";
         self#add_option "map_high" "25";
         self#add_option "stat_x" "25";
         self#add_option "stat_y" "17";
         self#add_option "stat_wide" "54";
         self#add_option "stat_high" "8";
         self#add_option "msg_x" "0";
         self#add_option "msg_y" "0";
         self#add_option "msg_wide" "54";
         self#add_option "msg_high" "32";
         self#add_option "menu_fc" "green";
         self#add_option "menu_bc" "grey";
         self#add_option "menu_hc" "cyan";
         self#add_option "shift_bq" "~";
         self#add_option "shift_1" "!";
         self#add_option "shift_2" "@";
         self#add_option "shift_3" "hash";
         self#add_option "shift_4" "$";
         self#add_option "shift_5" "%";
         self#add_option "shift_6" "^";
         self#add_option "shift_7" "&";
         self#add_option "shift_8" "*";
         self#add_option "shift_9" "(";
         self#add_option "shift_0" ")";
         self#add_option "shift_minus" "_";
         self#add_option "shift_equals" "+";
         self#add_option "shift_bs" "|";
         self#add_option "shift_[" "{";
         self#add_option "shift_]" "}";
         self#add_option "shift_;" ":";
         self#add_option "shift_'" "\"";
         self#add_option "shift_," "<";
         self#add_option "shift_." ">";
         self#add_option "shift_/" "?";
         self#add_option "key_get" ",";
         self#add_option "key_help" "?";
(*
         self#add_option "key_up" "keypad_north";
         self#add_option "key_down" "keypad_south";
         self#add_option "key_left" "keypad_west";
         self#add_option "key_right" "keypad_east";
         self#add_option "key_nw" "keypad_nw";
         self#add_option "key_ne" "keypad_ne";
         self#add_option "key_sw" "keypad_sw";
         self#add_option "key_se" "keypad_se";
         self#add_option "key_longwalk" "w";
         self#add_option "key_viewmap" "M";
         self#add_option "key_look" "l";
         self#add_option "key_quit" "Q";
         self#add_option "key_downstair" ">";
         self#add_option "key_upstair" "<";
         self#add_option "key_inventory" "i";
         self#add_option "key_drop" "d";
         self#add_option "key_multidrop" "D";
         self#add_option "key_attack" "a";
         self#add_option "key_previous" "-";
         self#add_option "key_next" "=";
         self#add_option "key_throw" "t";
         self#add_option "key_enter" ".";
         self#add_option "key_cast_spell" "Z";
         self#add_option "key_equipment" "e";
         self#add_option "key_drink" "q";
         self#add_option "key_read" "r";
         self#add_option "key_examine" "v";
         self#add_option "key_pass" "p";
*)
         self#add_option "fast_menu" "true";
         self#add_option "is_wizard" "false";
         self#add_option "show_debug" "false";
         self#add_option "message_reverse" "true";
         self#add_option "scenario" "Happy_hunting";
         self#add_option "fontfile" "vtsr____.ttf";
         self#add_option "font_points" "16";
         self#add_option "fullscreen" "false";
         self#add_option "expand" "false";
         self#add_option "expand_x" "0";
         self#add_option "expand_y" "0";
   end
;;

class loadlevel =
   object (self)
      inherit option
      method hard_init =
         self#add_option "material" "gfx/material1.png";
         self#add_option "background" "none";
         self#add_option "overlay" "none";
   end
;;

let get_option = 
   let topt = new option in
      topt#hard_init;
      if Sys.file_exists "vectorb.rc" then
         Uiopt_f.load_uiopt_file "vectorb.rc" topt;
   topt
;;

let get_level = 
   let level = new loadlevel in
      level#hard_init;
      if Sys.file_exists "level1.rc" then
         Uiopt_f.load_uiopt_file "level1.rc" level;
         level
;;
