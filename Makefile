NATIVECOMPILE = ocamlopt
COMPILE = ocamlc

NATIVEDEP = \
algo.cmx \
olevent.cmx \
s_colour.cmx \
key_sdl.cmx \
uiopt_f.cmx \
weapon.cmx \
collide.cmx \
player.cmx \
u_colour.cmx \
ui_opt.cmx \
ui_sdl.cmx \
uilink.cmx \
cursor.cmx \
boxes.cmx \
menulist.cmx \
api_ui.cmx \
vectorb.cmx

BYTEDEP = \
algo.cmo \
olevent.cmo \
s_colour.cmo \
key_sdl.cmo \
uiopt_f.cmo \
weapon.cmo \
collide.cmo \
player.cmo \
u_colour.cmo \
ui_opt.cmo \
ui_sdl.cmo \
uilink.cmo \
cursor.cmo \
boxes.cmo \
menulist.cmo \
api_ui.cmo \
vectorb.cmo

default : native

all : byte native

native : $(NATIVEDEP)
	ocamlfind $(NATIVECOMPILE) -package sdl \
	-package sdl.sdlttf \
	-package sdl.sdlimage \
	-o $@ -linkpkg $(NATIVEDEP)

byte : $(BYTEDEP)
	ocamlfind $(COMPILE) -package sdl \
	-package sdl.sdlttf \
	-package sdl.sdlimage \
	-o $@ -linkpkg $(BYTEDEP) -custom

vectorb.cmx : vectorb.ml api_ui.cmx player.cmx
	ocamlfind $(NATIVECOMPILE) -package sdl -package sdl.sdlimage \
	-c vectorb.ml


vectorb.cmo : vectorb.ml api_ui.cmo player.cmo
	ocamlfind $(COMPILE) -package sdl -package sdl.sdlimage \
	-c vectorb.ml

api_ui.cmx : api_ui.ml menulist.cmx boxes.cmx uilink.cmx player.cmx
	ocamlfind $(NATIVECOMPILE) -package sdl -package sdl.sdlimage \
	-c api_ui.ml

api_ui.cmo : api_ui.ml menulist.cmo boxes.cmo uilink.cmo player.cmo
	ocamlfind $(COMPILE) -package sdl -package sdl.sdlimage \
	-c api_ui.ml

menulist.cmx : menulist.ml
	$(NATIVECOMPILE) -c menulist.ml

menulist.cmo : menulist.ml
	$(COMPILE) -c menulist.ml

boxes.cmx : boxes.ml cursor.cmx
	$(NATIVECOMPILE) -c boxes.ml

boxes.cmo : boxes.ml cursor.cmo
	$(COMPILE) -c boxes.ml

cursor.cmx : cursor.ml
	$(NATIVECOMPILE) -c cursor.ml

cursor.cmo : cursor.ml
	$(COMPILE) -c cursor.ml

uilink.cmx : uilink.ml ui_sdl.cmx algo.cmx
	$(NATIVECOMPILE) -c uilink.ml

uilink.cmo : uilink.ml ui_sdl.cmo algo.cmo
	$(COMPILE) -c uilink.ml

ui_sdl.cmx : ui_sdl.ml ui_opt.cmx s_colour.cmx key_sdl.cmx cursor.cmx
	ocamlfind $(NATIVECOMPILE) -c -package sdl \
	ui_sdl.ml

ui_sdl.cmo : ui_sdl.ml ui_opt.cmo s_colour.cmo key_sdl.cmo cursor.cmo
	ocamlfind $(COMPILE) -c -package sdl \
	ui_sdl.ml

ui_opt.cmx : ui_opt.ml u_colour.cmx uiopt_f.cmx
	$(NATIVECOMPILE) -c ui_opt.ml

ui_opt.cmo : ui_opt.ml u_colour.cmo uiopt_f.cmo
	$(COMPILE) -c ui_opt.ml

u_colour.cmx : u_colour.ml
	$(NATIVECOMPILE) -c u_colour.ml

u_colour.cmo : u_colour.ml
	$(COMPILE) -c u_colour.ml

uiopt_f.cmx : uiopt_f.ml
	$(NATIVECOMPILE) -c uiopt_f.ml

uiopt_f.cmo : uiopt_f.ml
	$(COMPILE) -c uiopt_f.ml

s_colour.cmx : s_colour.ml
	ocamlfind $(NATIVECOMPILE) -package sdl -c s_colour.ml

s_colour.cmo : s_colour.ml
	ocamlfind $(COMPILE) -package sdl -c s_colour.ml

key_sdl.cmx : key_sdl.ml
	ocamlfind $(NATIVECOMPILE) -c -package sdl key_sdl.ml

key_sdl.cmo : key_sdl.ml
	ocamlfind $(COMPILE) -c -package sdl key_sdl.ml

olevent.cmo : olevent.ml
	$(COMPILE) -c olevent.ml

olevent.cmx : olevent.ml
	$(NATIVECOMPILE) -c olevent.ml

weapon.cmo : weapon.ml algo.cmo
	$(COMPILE) -c weapon.ml

weapon.cmx : weapon.ml algo.cmx
	$(NATIVECOMPILE) -c weapon.ml

collide.cmo : collide.ml weapon.cmo
	$(COMPILE) -c collide.ml

collide.cmx : collide.ml weapon.cmx
	$(NATIVECOMPILE) -c collide.ml

player.cmo : player.ml olevent.cmo collide.cmo weapon.cmo
	$(COMPILE) -c player.ml

player.cmx : player.ml olevent.cmx collide.cmx weapon.cmx
	$(NATIVECOMPILE) -c player.ml

algo.cmx : algo.ml
	$(NATIVECOMPILE) -c algo.ml

algo.cmo : algo.ml
	$(COMPILE) -c algo.ml

clean :
	rm -f *.cm* *.o byte native
