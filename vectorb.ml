(*
    Vector Battle
    Copyright (C) 2006 - 2010  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Provides the playloop for Vector Battle

*)

type game_structure =
 {
   arena_width : int;
   arena_height : int;
   screen_width : int;
   screen_height : int;
   bool_map : (bool * bool * bool) array array;
   gravity : float;
   mutable projectile : Weapon.projectile list;
   rope : Weapon.projectile list ref;
   mutable player : Player.player list;
   mutable sbitmask : (string * bool array array) list;
   mutable textblocks : (int * int * int * int) list;
   mutable weaponset : (string * Weapon.launcher) list;
   mutable bulletset : (string * Weapon.bullet_specs) list;
   mutable player_weapons : string list;
 }
;;

let proto_game_structure = 
{
   arena_width = 64;
   arena_height = 48;
   screen_width = 64;
   screen_height = 48;
   bool_map = Array.make_matrix 1 1 (false, false, false);
   gravity = 1000.0098;
   projectile = [];
   rope = ref [];
   player = [(Player.new_player "a" "a" 1.0 1.0 0) ];
   sbitmask = [];
   textblocks = [];
   bulletset = [];
   weaponset = [];
   player_weapons = [];
}
;;

let graphic_dim gname gstruct =
   let bmask = (List.assoc gname gstruct.sbitmask) in
   let width = Array.length bmask in
   let sample = Array.get bmask 0 in
   let height = Array.length sample in
      width, height
;;

let rec undraw_pixels ui points =
   match points with
      [] -> ()
    | (lx, ly) :: tail ->
         ui#cover_old_sprite "levelbox" "pixel" "static_bg" lx ly;
         undraw_pixels ui tail
;;

let rec ropes_of player ropes =
   match ropes with
      [] -> []
    | head :: tail ->
         if player#get_pin = head#get_owner then
            head :: ropes_of player tail
         else
            ropes_of player tail
;;

let rec find_player players pin =
   match players with 
      [] -> raise Not_found
    | head :: tail ->
         if head#get_pin = pin then head
         else find_player tail pin
;;

let undraw_rope ui hook players = 
  try
  (
   let owner = hook#get_owner in
   let player = find_player players owner in
      let lx, ly = hook#get_sprite_centre in
      let lx2, ly2 = player#get_sprite_centre in 
      let points = Algo.bresenham_line lx ly lx2 ly2 in
         undraw_pixels ui points
(*
         ui#remove_pixels "blah?why?FIXME" points
*)
  )
  with n -> raise (Failure ("in undraw_rope: " ^ Printexc.to_string n))
;;

let rec undisplay_projectiles ui pl players =
  try
  (
   match pl with
      [] -> []
    | head :: tail ->
         ui#cover_old_sprite "levelbox" head#get_graphic "static_bg" 
               head#get_sprite_locx head#get_sprite_locy;
         if head#is_rope then
               undraw_rope ui head players;
         if head#is_alive then
            head :: undisplay_projectiles ui tail players
         else
            undisplay_projectiles ui tail players
  )
  with n -> raise (Failure ("in undisplay_projectiles: " 
         ^ Printexc.to_string n))
;;

let rec undraw_textblocks ui tb =
  try
  (
   match tb with
      [] -> []
    | head :: tail ->
         ui#cover_box "levelbox" "static_bg" head;
         undraw_textblocks ui tail 
  )
  with n -> raise (Failure ("in undraw_textblocks: " 
         ^ Printexc.to_string n))
;;

let rec projectile_bitmask_coords pl =
   match pl with
      [] -> []
    | head :: tail ->
         head#get_bitmask_coords :: projectile_bitmask_coords tail
;;
(*
let rec explode_projectiles ui gname bool_map pl =
   let dirt_to_remove = projectile_bitmask_coords pl in
   ui#remove_pixels gname bool_map dirt_to_remove;
   next_explode_projectile pl
and next_explode_projectile pl =
   match pl with
      [] -> []
    | head :: tail ->
         next_explode_projectile tail
;;
*)

let draw_rope ui hook players = 
  try
  (
   let owner = hook#get_owner in
   let player = find_player players owner in
      let lx, ly = hook#get_sprite_centre in
(*
      let lx = hook#get_locx in let ly = hook#get_locy in
*)
      let lx2, ly2 = player#get_sprite_centre in
      let points = Algo.bresenham_line lx ly lx2 ly2 in
         ui#rope_pixels points
  )
  with n -> raise (Failure ("in draw_rope: " ^ Printexc.to_string n))
;;

let rec display_projectiles ui pl players =
  try
  (
   match pl with
      [] -> ()
    | head :: tail ->
         if head#is_alive then
           (
            let subbox = head#get_img_offset in
            ui#display_partial_graphic "levelbox" head#get_graphic 
                  head#get_locx head#get_locy subbox "arena";
            if head#is_rope then
               draw_rope ui head players
           );
         display_projectiles ui tail players
   )
   with n -> raise (Failure ("in undisplay_projectiles: " 
         ^ Printexc.to_string n))
;;

let rec remove_old_sprites ui uiopt gstruct players =
  try
  (
   match players with
      [] -> ()
    | player :: tail ->
      ui#cover_old_sprite "levelbox" player#get_wall_bitmask "static_bg" 
         player#int_sprite_locx
         player#int_sprite_locy;
      let width, height = player#get_dimensions in
      let hwdt = (width + 1) / 2 in
      let hhgt = (height + 1) / 2 in
      let cwidth, cheight = graphic_dim "crosshair" gstruct in
      let cwdt = (cwidth + 1) / 2 in
      let chgt = (cheight + 1) / 2 in
      let paim = player#get_sprite_aim_dir
      in
         let padx = (Algo.degsin paim) *. 30.0 in
         let pady = (Algo.degcos paim) *. 30.0 in
         let pposx = 
            int_of_float ((padx +. player#get_sprite_locx) +. 0.5) + 
            (hwdt - cwdt)
         in
         let pposy = 
            int_of_float ((pady +. player#get_sprite_locy) +. 0.5) + 
            (hhgt - chgt)
         in
         ui#cover_old_sprite "levelbox" "crosshair" "static_bg" 
            pposx pposy;
         remove_old_sprites ui uiopt gstruct tail
  ) with n -> raise (Failure ("in remove_old_sprites: " 
         ^ Printexc.to_string n))
;;

let gun_line aim distance =
   ((Algo.degsin aim) *. distance), ((Algo.degcos aim) *. distance)
;;
(*
let subpic_10x9 player =
      let x_graphic = (player#get_crawl_cycle * 10) +
         match player#get_facing with
            0 -> 1
          | _ -> 43
      in
      let y_graphic =
         ((int_of_float ((player#get_aim /. 22.5) +. 0.5)) * 9) + 1
      in
      x_graphic, y_graphic
;;
*)

let draw_crosshair ui gstruct player width height =
   let half_wdt = (width + 1) / 2 in 
   let half_hgt = (height + 1) / 2 in
   let cwidth, cheight = graphic_dim "crosshair" gstruct in
   let cwdt = (cwidth + 1) / 2 in 
   let chgt = (cheight + 1) / 2 in
   let aim = player#get_aim_dir in
(*      let cadx, cady = Algo.gun_line aim 5.0 in*)
      let adx, ady = Algo.gun_line aim 30.0 in
      let cposx = 
         int_of_float (((adx +. player#get_locx)) +. 0.5) + 
         (half_wdt - cwdt)
      in
      let cposy = 
         int_of_float (((ady +. player#get_locy)) +. 0.5) + 
         (half_hgt - chgt)
      in
         ui#display_graphic "levelbox" "crosshair" cposx cposy "arena"
;;

(* fixme - uiopt unneeded *)
let draw_player ui uiopt gstruct player =
   let width, height = player#get_dimensions in
      let x_graphic, y_graphic = player#get_img_offset in
      ui#display_partial_graphic "levelbox" player#get_graphic 
         player#int_locx player#int_locy 
         (x_graphic, y_graphic, width, height) "arena";
      draw_crosshair ui gstruct player width height;
      if player#get_display_selected_weapon then
         let weapon = player#get_weapon in
         let wname = Weapon.get_wname weapon in
         let dbox =
(*
            ui#free_say wname (player#int_locx - 10) 
                  (player#int_locy - 10) "arena"
*)
            ui#free_say_retbox wname (player#int_locx - 10) 
                  (player#int_locy - 10) "arena"
         in
            gstruct.textblocks <- dbox :: gstruct.textblocks
(*
      ui#display_graphic
            "levelbox" player#get_wall_bitmask 
             player#int_locx player#int_locy "arena"
*)
;;

let rec process_players ui uiopt gstruct gtime_step players =
   next_player ui uiopt gstruct gtime_step players 0 [] true
and next_player ui uiopt gstruct gtime_step players pcount acts 
      need_acts 
=
 try
 (
  match players with
    [] -> true
  | player :: tail ->
     (
      let playnow = ref true in
      let a1 = 
         if need_acts then
            ui#get_actions (* pcount *)
         else acts
      in
         playnow := (player#respond a1) ;
         player#drag gtime_step;
         player#gravity_pull gstruct.gravity gtime_step;
         let new_shots, new_ropes, ropes_unhooked =
               (player#events_take_effect 
                     gstruct.weaponset gstruct.sbitmask
                     gstruct.bool_map gstruct.arena_width 
                     gstruct.arena_height gstruct.gravity 
                     (* (ropes_of player *) gstruct.rope gtime_step
               )
         in
            let ropes_to_remove =
               Weapon.identify_duplicate_ropes new_ropes 
                     !(gstruct.rope) 
               @ ropes_unhooked
            in
            ignore (undisplay_projectiles ui ropes_to_remove players);
            gstruct.rope := new_ropes @ 
                  (Algo.remove_elements ropes_to_remove 
                   !(gstruct.rope) 
                  );
            gstruct.projectile <- new_shots @ gstruct.projectile;
         player#update_pos gstruct.bool_map 
            gstruct.sbitmask
            (gstruct.arena_width - 1)
            (gstruct.arena_height - 1)
            gtime_step;
         if !playnow then
            next_player ui uiopt gstruct gtime_step tail (pcount + 1) a1 
                  false
         else false
     )
 ) with n -> raise (Failure ("in process_players: " 
         ^ Printexc.to_string n))
;;

let proper_gametime_step rope projectile player base =
   let frope = Weapon.fastest rope in
   let fproj = Weapon.fastest projectile in
   let fplayer = Player.fastest player in
   let fastest = max fplayer (max frope fproj) in
      base /. ( max 1.0 (sqrt fastest))
;;

let rec draw_players ui uiopt gstruct players =
   match players with
      [] -> ()
    | player :: tail ->
         draw_player ui uiopt gstruct player;
         player#confirm_pos;
         draw_players ui uiopt gstruct tail
;;

let rec confirm_projectile_pos plist =
   match plist with
      [] -> ()
    | proj :: tail ->
         proj#confirm_pos;
         confirm_projectile_pos tail
;;

let update_screen ui uiopt playerlist gstruct gametime_taken 
      gametime_speed sum_elapsed pre_elapsed gtime_step
=
         sum_elapsed := 0;
         gstruct.projectile <- 
            undisplay_projectiles ui gstruct.projectile 
                  gstruct.player;
         gstruct.rope := 
            undisplay_projectiles ui !(gstruct.rope)
                  gstruct.player;
         remove_old_sprites ui uiopt gstruct playerlist;
         gstruct.textblocks <- undraw_textblocks ui gstruct.textblocks;
(* uncomment to see framerate in upper left corner *)
(*
      let elapsed = if pre_elapsed = 0 then 1 else pre_elapsed in
         let fps = 1000.0 /. (float_of_int elapsed) in
         let fps_string = (string_of_float fps)
            (* ^ "\n" ^ (string_of_int elapsed)^ "\n" ^
            (string_of_float gtime_step) ) *)
         in
         let dbox = ui#free_say_retbox fps_string 1 1 "arena" in
            gstruct.textblocks <- dbox :: gstruct.textblocks;
*)
         draw_players ui uiopt gstruct gstruct.player;
         display_projectiles ui gstruct.projectile gstruct.player;
         confirm_projectile_pos gstruct.projectile;
         confirm_projectile_pos !(gstruct.rope);
         display_projectiles ui !(gstruct.rope) gstruct.player;
         gametime_taken := (!gametime_taken -. gametime_speed);
         ui#clean_dirty;
         let p1 = List.nth gstruct.player 0 in
         let p2 = List.nth gstruct.player 1 in
         ui#dual_splitscreen_sync 
               (int_of_float p1#get_sprite_locx, 
                int_of_float p1#get_sprite_locy) 
               (int_of_float p2#get_sprite_locx, 
                int_of_float p2#get_sprite_locy) 
               gstruct.arena_width gstruct.arena_height p1 p2;
;;

let spawn player gstruct =
   let new_exp = 
      player#spawn 
            gstruct.bool_map 
            gstruct.arena_width gstruct.arena_height 
            gstruct.sbitmask 
   in
   gstruct.projectile <- gstruct.projectile
         @ Weapon.make_new_particles_mk2 gstruct.weaponset 
                 new_exp;
;;

let playloop ui uiopt playerlist gstruct =
  try
  (
   ui#display_graphic "levelbox" "static_bg" 0 0 "arena";
   let framestep_delay = 20 in
   let gametime_speed = 1.0 in
(*    let speed_variable = 0.5 in *)
   let mostp = ref 0 in
   let playnow = ref true in
   let ticks = ref (ui#get_ticks ()) in
   let sum_elapsed = ref 0 in
   let gametime_taken = ref 0.0 in
   while !playnow do
     (
      let gtime_step = proper_gametime_step 
            !(gstruct.rope) gstruct.projectile 
            gstruct.player gametime_speed
      in
      if gtime_step > 1.0 then print_string "!!!\n!!!!!!!\n";
      let new_projectiles =
         Collide.update_projectile ui gstruct.bulletset 
            gstruct.weaponset gstruct.sbitmask 
            gstruct.player 
            gstruct.bool_map
            gstruct.arena_width gstruct.arena_height 
            gstruct.gravity gtime_step gstruct.projectile
      in
      gstruct.projectile <- gstruct.projectile @ new_projectiles;
      let rope_projectiles = 
         Collide.update_projectile ui gstruct.bulletset
            gstruct.weaponset gstruct.sbitmask gstruct.player 
            gstruct.bool_map
            gstruct.arena_width gstruct.arena_height 
            gstruct.gravity gtime_step !(gstruct.rope)
      in
      (*unlikely to contain anything, the above was done for side 
            effects*)
      gstruct.rope := !(gstruct.rope) @ rope_projectiles;
      mostp := max !mostp (List.length gstruct.projectile);
      playnow := process_players ui uiopt gstruct gtime_step playerlist;
      let newticks = ui#get_ticks () in
      let pre_elapsed = newticks - !ticks in
      ticks := newticks;
      sum_elapsed := !sum_elapsed + pre_elapsed;
      gametime_taken := !gametime_taken +. gtime_step;
      if (!gametime_taken >= gametime_speed) 
      && (!sum_elapsed < framestep_delay) 
      then
        (
         let dv = (framestep_delay - !sum_elapsed) in
(*
            print_string ((string_of_int dv) ^ " = delay in ms\n");
*)
            ui#delay dv;
            sum_elapsed := framestep_delay
        );
(*
      else if (!sum_elapsed >= framestep_delay) then
         print_string ("no delay: " ^ (string_of_int !sum_elapsed)
               ^ "\n");
*)
      if ((!gametime_taken >= gametime_speed) 
      && (!sum_elapsed >= framestep_delay) )
      || (!sum_elapsed > ((framestep_delay * 2) - 1)) then
        (
         update_screen ui uiopt playerlist gstruct gametime_taken 
               gametime_speed sum_elapsed pre_elapsed gtime_step
        )
     )
   done;
   print_string ("\n" ^ string_of_int !mostp ^ " particles\n");
  )
  with n -> raise (Failure ("in playloop: " ^ Printexc.to_string n))
;;

let rec make_large_bitmask small_bitmask =
   match small_bitmask with
      [] -> []
    | (ind, head) :: tail -> 
         (ind, (next_large_bitmask head)) :: (make_large_bitmask tail)
and next_large_bitmask small =
   let len_x = Array.length small in
   let sample_y = Array.get small 0 in
   let len_y = Array.length sample_y in
   let large = Array.make_matrix (len_x + 2) (len_y + 2) false in
   for countx = 0 to (len_x - 1) do
      let col = Array.get small countx in
      let left = Array.get large countx in
      let right = Array.get large (countx + 2) in
      let middle = Array.get large (countx + 1) in
      for county = 0 to (len_y - 1) do
         let cell = Array.get col county in
            if cell then
              (
               Array.set left (county + 1) true;
               Array.set right (county + 1) true;
               Array.set middle county true;
               Array.set middle (county + 1) true;
               Array.set middle (county + 2) true;
              )
      done
   done;
   large
;;

let load_gamestruct ui matname bgname screenx screeny =
   ui#new_graphic_box "loadbox" 0 0 screenx screeny;
   ui#load_graphic "loadscreen" "gfx/loading.png";
   ui#display_graphic "loadbox" "loadscreen" 0 0 "arena";
   ui#sync;
   ui#load_graphic "material" matname;
(* FIXME
   ui#load_graphic "material" matname;
*)
   ui#load_graphic "background" bgname;
(* make sure nobody can fall through the bottom of the screen: *)
   ui#conditional_underline_graphic_rock "material"; 
   ui#colour_material "background";
   let imagex, imagey = ui#get_gdim "material" in
   ui#init_arena imagex imagey;
   let bullets, _ = 
      Weapon.load_bulletset "bullets.rc" "data/bullets/" 
   in
   let (weapons, weaponlist) = 
      Weapon.load_weaponset "weapons.rc" "data/weapons/" bullets 
   in
   let (explosions, _) =
      Weapon.load_weaponset "explosions.rc" "data/weapons/" bullets
   in
   let allweapons = ("proto_dig", Weapon.proto_dig_l)
      :: weapons @ explosions 
   in
   {
      arena_width = imagex;
      arena_height = imagey;
      screen_width = screenx;
      screen_height = screeny;
      gravity = (0.0098);
      bool_map = Array.make_matrix imagex imagey (false, false, false);
      projectile = [];
      rope = ref [];
      player = [(Player.new_player "a" "a" 1.0 1.0 0) ];
      sbitmask = [];
      textblocks = [];
      bulletset = bullets;
      weaponset = allweapons;
      player_weapons = weaponlist;
   }
;;

class session = 
  let ui_opt = Ui_opt.get_option in
  let level = Ui_opt.get_level in
  let ui = Api_ui.new_toplevel_ui ui_opt in
  object(self)
   val mutable gstruct = 
         load_gamestruct ui (level#get_key "material") "gfx/bg1.png"
               (ui_opt#get_int "screen_width") 
               (ui_opt#get_int "screen_height")
   method menu =
      let item1 = Menulist.strn_menu_item "heading" "Choose:" in
      let item2 = Menulist.strn_menu_item "option" "Begin" in
      let item3 = Menulist.strn_menu_item "option" "Setup" in
         let m = Menulist.make_menu [item1; item2; item3] in
         let h = ui#box_mimic_ret_height "temp" 
             (ui_opt#get_key "big_window")
         in
         let choice = ui#choose_menuitem ui_opt m h in
            print_string (choice#get_content)
   method load_graphic gname gpath =
      ui#load_graphic gname gpath;
      gstruct.sbitmask <-
         (gname, (ui#make_bitmask gname)) 
         :: gstruct.sbitmask
   method load_graphics =
         ui#new_graphic_box 
            "master" 0 0 gstruct.screen_width gstruct.screen_height;
         ui#new_graphic_box 
            "levelbox" 0 0 gstruct.arena_width gstruct.arena_height;
         let raw = Uiopt_f.load_weapon_file "data/gfx.rc" in
         let prefix = "gfx/" in
         for count = 0 to (List.length raw) - 1 do
            let key, dat = List.nth raw count in
               self#load_graphic key (prefix ^ dat)
         done
   method begin_game =
      try
        (
         Random.self_init ();
         self#load_graphics;
(*  does not work for some reason
         ui#set_alpha "healthbar_ends" 16;
*)
         ui#fill_map gstruct.bool_map "material"; 
         ui#colour_material "material";
         ui#create_composite "static_bg" "background" "material";
         let p1 = List.nth gstruct.player 0 in
            p1#init "worm" "wallbar" "proto_dig"
                  150.0 170.0 0;
         let p2 = Player.new_player "worm" "wallbar" 
               570.0 160.0 1 
         in
            p1#set_img_width 10; p1#set_img_height 9;
            p2#set_img_width 10; p2#set_img_height 9;
            p2#init "worm" "wallbar" 
                  "proto_dig" 570.0 160.0 1;
            for count = 0 to 
               ((List.length gstruct.weaponset) - 1) 
            do
               let (lname, wpn) = List.nth gstruct.weaponset count in
               if List.mem lname gstruct.player_weapons then
                 (
                  p1#add_weapon (Weapon.new_weapon wpn); 
                  p2#add_weapon (Weapon.new_weapon wpn);
                 )
(*
               else print_string ("subweapon: " ^ lname ^ "\n");
*)
            done;
            for count = 0 to ((List.length gstruct.player_weapons) - 1) 
            do
               let lname = List.nth gstruct.player_weapons count in
               print_string ("Player weapon: " ^ lname ^ "\n")
            done;
(*
         let p3 = Player.new_pixelstepper "pixel" 600.0 100.0 2 in
            p3#set_img_width 7; p3#set_img_height 7;
*)
            gstruct.player <- (*p3 ::*) p2 :: gstruct.player;
(*         self#menu; *)
(*
            let p1x, p1y = p1#centre_loc in
            let p2x, p2y = p2#centre_loc in
            let new_exps = [
                  ([p1#get_dig_img], p1x, p1y, 0., 0.,
                     p1#get_pin);
                  ([p2#get_dig_img], p2x, p2y, 0., 0.,
                     p2#get_pin);
                  ]
            in
            gstruct.projectile <- gstruct.projectile
                  @ Weapon.make_new_particles_mk2 gstruct.weaponset 
                          new_exps;
*)
         spawn p1 gstruct; spawn p2 gstruct;
         playloop ui ui_opt gstruct.player gstruct;
        )
      with e ->
        (
         print_string ("\n\nError: Program failed on exception: \n"
                       ^ (Algo.exception_string (Printexc.to_string e)) 
                       ^ "\n\n") ;
         ui#end_ui
        )
   end
;;

let s = new session;;

s#begin_game;;
