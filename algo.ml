(*
    Vector Battle
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    Provides implementations for a bunch of common algorithms. 

*)

let step_dir d =
   if d < 0 then -1
   else 1
;;

let rec x_step cx cy ex ey step_x step_y half_x dx dy error_count
=
   if (cx, cy) = (ex, ey) then [(cx, cy)]
   else
      let new_error = ref (error_count + dy) in
         let (new_cx, new_cy) =
            if !new_error > half_x then
               (new_error := !new_error - dx;
                ((cx + step_x), (cy + step_y))
               )
            else ((cx + step_x), cy)
         in
            (cx, cy) ::
            (x_step new_cx new_cy ex ey step_x step_y half_x dx dy
                    !new_error)
;;

let rec y_step cx cy ex ey step_x step_y half_y dx dy error_count
=
   if (cx, cy) = (ex, ey) then [(cx, cy)]
   else
      let new_error = ref (error_count + dx) in
         let (new_cx, new_cy) =
            if !new_error > half_y then
               (new_error := !new_error - dy;
                ((cx + step_x), (cy + step_y))
               )
            else (cx, (cy + step_y))
         in
            (cx, cy) ::
            (y_step new_cx new_cy ex ey step_x step_y half_y dx dy 
                    !new_error)
;;

let bresenham_line lx1 ly1 lx2 ly2 =
   let (dx, dy) = ((lx2 - lx1), (ly2 - ly1)) in
      let (step_x, step_y, new_dx, new_dy) =
         ((step_dir dx), (step_dir dy), (abs dx), (abs dy))
      in
         if new_dx > new_dy then
            let half_x = new_dx / 2 in
               x_step lx1 ly1 lx2 ly2 step_x step_y half_x new_dx new_dy
                      0
         else
            let half_y = new_dy / 2 in
               y_step lx1 ly1 lx2 ly2 step_x step_y half_y new_dx new_dy
                      0
;;

let deg2rad v = (v -. 180. ) *. 6.28318530717958623 /. 360.;;

let rad2deg v = ((v /. 6.28318530717958623) *. 360.) +. 180.;;

let degcos v = cos (deg2rad v);;

let degsin v = sin (deg2rad v);;

let degtan v = tan (deg2rad v);;

let degatan v = rad2deg (atan v);;

let rec list_without l elem =
   match l with
      [] -> []
    | head :: tail ->
         if head = elem then
            list_without tail elem
         else head :: list_without tail elem
;;

let rec remove_elements elems l =
   match elems with
      [] -> l
    | head :: tail ->
         remove_elements tail (list_without l head)
;;

let round_int n =
   int_of_float (n +. 0.5)
;;

let gun_line aim distance =
   ((degsin aim) *. distance), ((degcos aim) *. distance)
;;

let vector x y = 
   let sx = abs_float x in let sy = abs_float y in
(*
   let a_t = degatan (sy /. sx) in
*)
      if (x >= 0.) && (y >= 0.) then 
         ((degatan (sy /. sx)) -. 90.)
      else if (x >= 0.) && (y < 0.) then 
         ((degatan (sx /. sy)) -. 180.)
      else if (x < 0.) && (y >= 0.) then 
         (degatan (sx /. sy))
      else if (x < 0.) && (y < 0.) then 
         ((degatan (sy /. sx)) +. 90.)
      else ( print_string "impossible..."; -1000.)
;;

let rec splitlist l this =
   match l with
      [] -> []
    | head :: tail ->
         if this then head :: splitlist tail false
         else splitlist tail true
;;

let rec add_offset offx offy l =
   match l with
      [] -> []
    | (x, y) :: tail ->
         ((offx + x), (offy + y)) :: add_offset offx offy tail
;;

let exception_string s =
   let l = String.length s in
      for count = 0 to (l - 1) do
         if s.[count] = "\\".[0] then s.[count] <- " ".[0]
      done;
   s
;;

let rec minima_maxima l = 
   next_mm l (-1) (-1) 0 0 (ref false) (ref false) (ref false) 
         (ref false)
and next_mm l min_x min_y max_x max_y iminx iminy imaxx imaxy =
   match l with
      [] -> min_x, min_y, max_x, max_y
    | (x, y) :: tail ->
         let nminx = if (not !iminx) then 
           (
            iminx := true;
            x
           )
         else (min x min_x) in
         let nminy = if (not !iminy) then 
           (
            iminy := true;
            y
           )
         else (min y min_y) in
         let nmaxx = if (not !imaxx) then 
           (
            imaxx := true;
            x
           )
         else (max x max_x) in
         let nmaxy = if (not !imaxy) then 
           (
            imaxy := true;
            y 
           )
         else (max y max_y) in
         next_mm tail nminx nminy nmaxx nmaxy iminx iminy imaxx imaxy
;;
