(*
    Jude's User Interface
    Copyright (C) 2004-2009  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What does this file do?

**    ui option file handling

*)


let wherein_string s c =
   let loc = ref (-1) in
      for count = 0 to (String.length s - 1) do
         if s.[count] = c then loc := count
      done;
   !loc
;;

let count_whitespace line =
   let wcount = ref 0 in
      for lcount = 0 to (String.length line - 1) do
         if line.[lcount] = ' ' then
            wcount := !wcount + 1
      done;
    !wcount
;;

let remove_whitespace line = 
   let offset = ref 0 in
   let wc = count_whitespace line in
      let newstring = String.create (String.length line - wc) in
         for count = 0 to (String.length line - 1) do
            match line.[count] with
               ' ' -> offset := !offset + 1
             | _ -> newstring.[(count - !offset)] <- line.[count]
         done;
      newstring
;;

let breakup_line line =
   let cline = remove_whitespace line in
      let brokenl = [] in
         match String.contains cline '=' with
            true -> 
               let bpos = wherein_string cline '=' in
                  String.sub cline 0 bpos ::
                  String.sub cline (bpos + 1) 
                                   (String.length cline - (bpos + 1)) 
                                          :: brokenl
          | false -> raise Not_found
;;

let optfile_process_line arg option =
   let a0 = List.nth arg 0 in
      let a1 = List.nth arg 1 in
         if option#has_opt a0 then option#add_option a0 a1
;;

let weaponfile_process_line line =
   let brokenl = breakup_line line in
      match List.length brokenl with
         2 -> (List.nth brokenl 0), (List.nth brokenl 1)
       | _ -> raise Not_found
;;

let process_line line option f_process =
   let brokenl = breakup_line line in
      match List.length brokenl with
         2 -> f_process brokenl option
       | _ -> raise Not_found
;;

let process_infile_opt infile option =
   (let complete = ref 0 in
      while !complete = 0 do
         try
            let line = input_line infile in
               if (String.contains line '#') = false then
                  process_line line option optfile_process_line
         with 
            End_of_file ->
               complete := 1
          | Not_found -> ()
      done;
   )
;;

let process_infile_weapon infile =
   let result = ref ([]:(string * string) list) in
   let complete = ref 0 in
      while !complete = 0 do
         try
            let line = input_line infile in
               if (String.contains line '#') = false then
                  result := weaponfile_process_line line :: !result
         with 
            End_of_file ->
               complete := 1
          | Not_found -> ()
      done;
   !result
;;

let load_uiopt_file filename option =
   let infile = open_in filename in
      process_infile_opt infile option
;;

let load_weapon_file filename =
   print_string ("Loading file " ^ filename ^"\n");
   try 
      let infile = open_in filename in
         process_infile_weapon infile
   with n -> 
     (
      print_string ((Printexc.to_string n) ^ "\n");
      []
     )
;;

let write_uiopt_file filename option version =
   ()
;;
